//
//  Constants.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 18.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import Foundation


struct Constant {
    static let clientId = "0dc11b34ed91a94fb03ecf3cd2b7d659d2ec1c22e9d78592da76f136320b40ce"
    static let clientSecret = "6e8bb0e23b73f15220c1fcc62b13a00c9bdcba663fe8247632e888d19c387f4c"
    static let redirectURI = "scctrakt://auth/trakt"
}
