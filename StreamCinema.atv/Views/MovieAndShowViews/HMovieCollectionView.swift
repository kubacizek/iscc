//
//  HMovieCollectionView.swift
//  StreamCinema.atv
//
//  Created by SCC on 15/12/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

struct RelatedMovieModel {
    var id: String
    var poster: URL?
    var fanArt: URL?
    var logo: URL?
    var trailer: String?
    var title: String?
    var desc: String?
    var movieData: [[String : String]]
    var ratingData: [[String : String]]
    var state: WatchedState?
    var type: MediaType
    var traktID: String?
    var episode: Int?
    var season: Int?
    var old:InfoData
    
    func getOneLineRating() -> String? {
        for item in self.ratingData {
            if let csfd = item["csfd"] {
                return csfd
            } else {
                return "\(item.keys.first ?? ""): \(item.values.first ?? "")"
            }
        }
        return nil
    }
}

//MARK: - Fanart and TMDB images
import TMDBKit
import FanArtKit
extension RelatedMovieModel {
    public func cellBacgroundImage(completition: @escaping (URL?)->Void) {
        if let fanArt = self.fanArt,
           !fanArt.absoluteString.contains("null") {
            completition(fanArt)
        } else {
            self.fetchBacgroundImage(completition: completition)
        }
    }
    
    private func fetchBacgroundImage(completition: @escaping (URL?)->Void) {
        self.tmdbBackdrop { url in
            if let url = url {
                completition(url)
            } else {
                self.fanArtThumb(completition: completition)
            }
        }
    }
    
    private func tmdbBackdrop(completition: @escaping (URL?)->Void ) {
        if let tmdb = self.old.source?.services?.tmdb,
           let tmdbID = Int(tmdb) {
            MovieMDB.movie(movieID: tmdbID) { (value, find) in
                if let backdrop = value.json?["backdrop_path"],
                   backdrop != nil,
                   let backdropURL = URL(string: "https://image.tmdb.org/t/p/w500\(backdrop)"){
                    completition(backdropURL)
                }
                else if let thumb = find?.belongs_to_collection?.backdrop_path,
                   let backdrop = URL(string: "https://image.tmdb.org/t/p/w500\(thumb)") {
                    completition(backdrop)
                } else {
                    completition(nil)
                }
            }
        }
    }
    
    private func fanArtThumb(completition: @escaping (URL?)->Void) {
        if let tmdb = self.old.source?.services?.tmdb,
           let tmdbID = Int(tmdb) {
            FanartClient.shared.movie(tmdb: tmdbID) { fanArtResult -> (Void) in
                switch fanArtResult {
                case .success(result: let result):
                    Log.write("fanArtThumb \(result)")
                    if let strURL = result.moviethumb?.first?.url {
                        completition(URL(string: strURL))
                    } else {
                        completition(nil)
                    }
                case .error(reason: let reason):
                    Log.write("getFanArt reason \(reason)")
                    completition(nil)
                }
            }
        } else {
            completition(nil)
        }
    }
}

extension InfoData {
    func getViewModel() -> RelatedMovieModel? {
        guard let id = self.id,
              let infoLabel = self.source?.getInfoLabels(),
              let type = self.source?.infoLabels?.getMediaType()
        else { return nil }
        
        let desc = infoLabel.plot
        var title = infoLabel.title
        if title == nil {
            switch type {
            case .movie,
                 .tvshow:
                break
            case .season:
                title = String(localized: .season)
                if let seasionNumber = self.source?.infoLabels?.season {
                    title = "\(seasionNumber) - \(String(localized: .season))"
                }
            case .episode:
                title = String(localized: .episode)
                if let episodeNumber = self.source?.infoLabels?.episode {
                    title = "\(episodeNumber) - \(String(localized: .episode))"
                }
            }
        }
        
        let poster = infoLabel.art?.poster ?? ""
        let fanArt = infoLabel.art?.fanart ?? ""
        let clearlogo = infoLabel.art?.clearlogo ?? ""
        
        let movieData = self.source?.getMovieData() ?? [[:]]
        let ratingData = self.source?.getAllRatings() ?? [[:]]
        let trailer = self.source?.getTrailer()
        let traktID = self.source?.services?.trakt
        let episode = self.source?.infoLabels?.episode
        let season = self.source?.infoLabels?.season
        
        return RelatedMovieModel(id: id, poster: URL(string: poster) , fanArt: URL(string: fanArt), logo: URL(string: clearlogo), trailer: trailer, title: title, desc: desc, movieData: movieData,ratingData: ratingData, state: nil, type: type, traktID:traktID, episode:episode, season:season, old: self)
    }
}

protocol HMovieCollectionViewDelegate: class {
    func hMovieCollection(_ hMovieCollection: HMovieCollectionView, movie:SCCMovie)
    func hMovieCollection(_ hMovieCollection: HMovieCollectionView, focusAt indexPath:IndexPath)
    func hMovieCollectionNextPage(_ hMovieCollection: HMovieCollectionView)
}

final class HMovieCollectionView: UICollectionView {
    var model:[SCCMovie] = []
    weak var hMovieDelegate: HMovieCollectionViewDelegate?
    var selectedItem:IndexPath?
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.configure()
    }
    
    private func configure() {
        self.register(UINib(nibName: "EpisodeCell", bundle: nil), forCellWithReuseIdentifier: "EpisodeCell")
        self.delegate = self
        self.dataSource = self
        self.remembersLastFocusedIndexPath = true
    }
    
}

extension HMovieCollectionView: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.model.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EpisodeCell", for: indexPath) as? EpisodeCell,
           indexPath.row < self.model.count {
            var data = self.model[indexPath.row]
            data.state = data.getCurrentState()
            cell.set(movie: data)
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row < self.model.count {
            let current = self.model[indexPath.row]
            self.hMovieDelegate?.hMovieCollection(self, movie: current)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldUpdateFocusIn context: UICollectionViewFocusUpdateContext) -> Bool {
        if let indexPath = context.nextFocusedIndexPath {
            self.hMovieDelegate?.hMovieCollection(self, focusAt: indexPath)
        }
        return true
    }
    
    func indexPathForPreferredFocusedView(in collectionView: UICollectionView) -> IndexPath? {
        return self.selectedItem
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if self.model.count != 0,
           (self.model.count - 1) == indexPath.row {
            self.hMovieDelegate?.hMovieCollectionNextPage(self)
        }
    }
}
