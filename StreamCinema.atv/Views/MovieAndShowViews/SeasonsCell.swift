//
//  SeasonsCell.swift
//  StreamCinema.atv
//
//  Created by SCC on 14/12/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

protocol SeasonsCellDelegate: class {
    func movie(_ cell:UITableViewCell, didSelect movie: SCCMovie)
    func movie(_ cell:UITableViewCell, focused model:SCCMovie)
    func movie(_ cell:UITableViewCell, for nextPage: MovieViewCells?)
}

final class SeasonsCell: UITableViewCell, MovieDetailCellProtocol {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var seasonCollectionView: HMovieCollectionView!
    @IBOutlet weak var seasonDesc: UILabel!
    
    var type: MovieViewCells?
    weak var delegate:SeasonsCellDelegate?
    var model: [SCCMovie] = [] {
        didSet {
            if self.seasonCollectionView != nil {
                self.seasonCollectionView.model = self.model
                self.seasonCollectionView.hMovieDelegate = self
            }
            self.seasonCollectionView.reloadData()
        }
    }
    
    public func selected(indexPath:IndexPath) {
        self.seasonCollectionView.selectedItem = indexPath
    }
    
    private func setDesc(for row:Int, showCount:Bool = false) {
        if let type = self.type {
            if type == .episodes {
                self.titleLabel.text = String(localized: .episode)
            } else  if type == .seasons {
                self.titleLabel.text = String(localized: .season)
            }
        }
        
        if row < self.model.count {
            let data = self.model[row]
            
            var titleString = ""
            titleString = data.title + "\n"
            if showCount {
                titleString = "\(row + 1) - " + data.title + "\n"
            }
            self.titleLabel.text = titleString
            
            let title = NSMutableAttributedString(string: "\(titleString)",
                                                attributes: [.foregroundColor:UIColor.label,
                                                             NSAttributedString.Key.font:UIFont.systemFont(ofSize: 20, weight: .medium)])
            
            let desc = NSMutableAttributedString(string: "\(data.desc ?? "")",
                                                attributes: [.foregroundColor:UIColor.label,
                                                             NSAttributedString.Key.font:UIFont.systemFont(ofSize: 20, weight: .regular)])
            title.append(desc)
            self.seasonDesc.attributedText = title
        }
    }
}

extension SeasonsCell: HMovieCollectionViewDelegate {
    func hMovieCollectionNextPage(_ hMovieCollection: HMovieCollectionView) {
        self.delegate?.movie(self, for: self.type)
    }
    
    func hMovieCollection(_ hMovieCollection: HMovieCollectionView, focusAt indexPath: IndexPath) {
        if indexPath.row < self.model.count {
            let data = self.model[indexPath.row]
            self.setDesc(for: indexPath.row, showCount: data.traktType == .episode)
            self.delegate?.movie(self, focused: data)
        }
    }
    
    func hMovieCollection(_ hMovieCollection: HMovieCollectionView, movie:SCCMovie) {
        self.delegate?.movie(self, didSelect: movie)
    }
}
