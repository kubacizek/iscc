//
//  MovieView.swift
//  StreamCinema.atv
//
//  Created by SCC on 15/12/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

enum MovieViewCells {
    case root
    case seasons
    case episodes
    case cast
    case releatedMovie
    case releatedShows
}

struct Selected {
    var season:Int
    var episode:Int
}

final class MovieModel: NSObject {
    var rootMovie:SCCMovie? {
        didSet {
            if let type = self.rootMovie?.traktType {
                self.type = type
            }
        }
    }
    var seasons:[SCCMovie] = []
    var episodes:[SCCMovie] = []
    var casts:[Cast] = []
    var releatedMovie:[SCCMovie] = []
    var releatedTvShows:[SCCMovie] = []
    var selected:Selected = Selected(season: 1, episode: 1)
    var type: SCCType = .tvshow
    
    func getData(_ cell:MovieViewCells) -> [Any] {
        switch cell {
        case .root:
            guard let root = self.rootMovie else { return [] }
            return [root]
        case .seasons:
            return self.seasons
        case .episodes:
            return self.episodes
        case .cast:
            return self.casts
        case .releatedMovie:
            return self.releatedMovie
        case .releatedShows:
            return self.releatedTvShows
        }
    }
    
    var cells: [MovieViewCells] {
        get {
            var cells:[MovieViewCells] = [.root]
            if seasons.count > 0 {
                cells.append(.seasons)
            }
            if self.episodes.count > 0 {
                cells.append(.episodes)
            }
            if self.casts.count > 0 {
                cells.append(.cast)
            }
            if self.releatedMovie.count > 0 {
                cells.append(.releatedMovie)
            }
            if releatedTvShows.count > 0 {
                cells.append(.releatedShows)
            }
            return cells
        }
    }
    
    func existInWatchList() -> Bool {
        if let exist = self.rootMovie?.isExistInWatchList(), exist {
            return true
        }
        return false
    }
    
    func continueButtonText() -> String? {
        let state = self.rootMovie?.getCurrentState()
        guard let rootMovie = self.rootMovie else { return nil }
        if rootMovie.traktType == .movie {
            
            if let state = state,
               state == .done || state == .none {
                return nil
            }
            if let time = rootMovie.time {
                let duration = TimeInterval(time)
                return duration.formatedString()
            }
            return ""
        }
        
        if let lastPayedEpisode = WatchedWrapper.lastPlayedEpisode(for: rootMovie.ids),
           let episode = lastPayedEpisode.episode {
            var episodeString = ""
            if let season = lastPayedEpisode.season {
                episodeString = "\(String(localized: .season)): \(season)"
            }
            episodeString = episodeString + " \(String(localized: .episode)): \(episode)"
            return episodeString
        }
        return nil
    }
    
}

enum MovieViewAction: Int {
    case playMovie
    case continueMovie
    case addToWatchList
    case removeFromWatchList
    case showCast
    case showRelated
}

protocol MovieViewDelegate: class {
    func movieView(_ movieView:MovieView, showDetailFor movie:SCCMovie)
    func movieView(_ movieView:MovieView, showDetailFor cast:Cast)
    func movieView(_ movieView:MovieView, focused item:SCCMovie)
    func movieView(_ movieView:MovieView, didSelect item:SCCMovie)
    func movieView(_ movieView:MovieView, didPerfom action:MovieViewAction)
    func movieView(_ movieView:MovieView, for nextPage: MovieViewCells?)
}

final class MovieView: UITableView {
    public weak var movieDelegate:MovieViewDelegate?
    private var currentCells: [MovieViewCells] = []
    var model: MovieModel? {
        didSet {
            if let model = model {
                self.currentCells = model.cells
            }
            self.configure()
            self.reloadData()
        }
    }
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        self.registerCells()
        self.configure()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.registerCells()
        self.configure()
    }
    
    
    public func registerCells() {
        self.register(UINib(nibName: "TopMovieDetailCell", bundle: nil), forCellReuseIdentifier: "TopMovieDetailCell")
        self.register(UINib(nibName: "RelatedMoviesCell", bundle: nil), forCellReuseIdentifier: "RelatedMoviesCell")
        self.register(UINib(nibName: "SeasonsCell", bundle: nil), forCellReuseIdentifier: "SeasonsCell")
        self.register(UINib(nibName: "ArtistsCell", bundle: nil), forCellReuseIdentifier: "ArtistsCell")
    }
    
    private func configure() {
        if self.delegate == nil {
            self.delegate = self
            self.dataSource = self
        }
    }
}

extension MovieView: UITableViewDelegate, UITableViewDataSource  {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.currentCells.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 384
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard self.currentCells.count > indexPath.row else { return UITableViewCell() }
        
        let currentCell = self.currentCells[indexPath.row]
        switch currentCell {
        case .root:
            if let data = self.model?.getData(currentCell).first as? SCCMovie,
               let cell = tableView.dequeueReusableCell(withIdentifier: "TopMovieDetailCell") as? TopMovieDetailCell {
                cell.set(title: data.title, movieDesc: data.desc ?? "", fanArt: data.fanart, logo: data.logo, movieData: data.movieData, ratingData:data.rating)
                cell.delegate = self
                cell.setContinue(title: self.model?.continueButtonText(), isInWatchList: self.model?.existInWatchList())
                cell.type = currentCell
                return cell
            }
        case .seasons:
            if let data = self.model?.getData(currentCell) as? [SCCMovie],
               let cell = tableView.dequeueReusableCell(withIdentifier: "SeasonsCell") as? SeasonsCell {
                cell.delegate = self
                cell.model = data
                cell.type = currentCell
                if let model = self.model,
                   let seasonIndex = model.seasons.firstIndex(where: { $0.season == model.selected.season }) {
                    let path = IndexPath(row: seasonIndex, section: 0)
                    cell.selected(indexPath:path)
                }
                return cell
            }
        case .episodes:
            if let data = self.model?.getData(currentCell) as? [SCCMovie],
               let cell = tableView.dequeueReusableCell(withIdentifier: "SeasonsCell") as? SeasonsCell {
                cell.delegate = self
                cell.model = data
                cell.type = currentCell
                if let model = self.model,
                   let episodeIndex = model.seasons.firstIndex(where: { $0.episode == model.selected.episode }) {
                    let path = IndexPath(row: episodeIndex, section: 0)
                    cell.selected(indexPath:path)
                }
                return cell
            }
        case .cast:
            if let data = self.model?.getData(currentCell) as? [Cast],
               let cell = tableView.dequeueReusableCell(withIdentifier: "ArtistsCell") as? ArtistsCell {
                cell.delegate = self
                cell.model = data
                cell.type = currentCell
                return cell
            }
        case .releatedMovie:
            if let data = self.model?.getData(currentCell) as? [SCCMovie],
               let cell = tableView.dequeueReusableCell(withIdentifier: "RelatedMoviesCell") as? RelatedMoviesCell {
                cell.delegate = self
                cell.model = data
                cell.type = currentCell
                return cell
            }
        case .releatedShows:
            if let data = self.model?.getData(currentCell) as? [SCCMovie],
               let cell = tableView.dequeueReusableCell(withIdentifier: "RelatedMoviesCell") as? RelatedMoviesCell {
                cell.delegate = self
                cell.model = data
                cell.type = currentCell
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, canFocusRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
}

extension MovieView: TopMovieDetailCellDelegate {
    func topMovieDetail(_ cell: TopMovieDetailCell, didPerform action: MovieViewAction) {
        self.movieDelegate?.movieView(self, didPerfom: action)
    }
}

extension MovieView: RelatedMoviesCellDelegate {
    func relatedMoviesCell(_ relatedMoviesCell: RelatedMoviesCell, didSelect movie: SCCMovie) {
        self.movieDelegate?.movieView(self, showDetailFor: movie)
    }
}

extension MovieView: SeasonsCellDelegate {
    func movie(_ cell:UITableViewCell, for nextPage: MovieViewCells?) {
        self.movieDelegate?.movieView(self, for: nextPage)
    }
    
    func movie(_ cell: UITableViewCell, didSelect movie: SCCMovie) {
        self.movieDelegate?.movieView(self, didSelect: movie)
    }
    
    func movie(_ cell:UITableViewCell, focused model:SCCMovie) {
        self.movieDelegate?.movieView(self, focused: model)
    }
}

extension MovieView: ArtistsCellDelegate {
    func artistsCell(_ artistsCell: ArtistsCell, didSelect artist: Cast) {
        self.movieDelegate?.movieView(self, showDetailFor: artist)
    }
}
