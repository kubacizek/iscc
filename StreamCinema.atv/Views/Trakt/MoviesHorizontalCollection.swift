//
//  MoviesHorizontalCollection.swift
//  StreamCinema.atv
//
//  Created by SCC on 19/01/2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit

protocol MoviesHorizontalCollectionDelegate: class {
    func moviesHorizontal(collection:MoviesHorizontalCollection, didSelect item:SCCMovie)
    func moviesHorizontalNextPage(_ collection:MoviesHorizontalCollection)
}

final class MoviesHorizontalCollection: UICollectionView {
    private(set) var model: [SCCMovie] = []
    public weak var movieDelegate:MoviesHorizontalCollectionDelegate?
    
    public func set(_ model: [SCCMovie]) {
        self.model = model
        self.reloadData()
        self.delegate = self
        self.dataSource = self
    }
    
    public func updete(_ model: [SCCMovie]) {
        let oldLastIndex = self.model.count - 1
        let newLastIndex = model.count - 1
        self.model = model
        self.reloadData()
//        guard oldLastIndex > 0, newLastIndex > oldLastIndex else { return }
//        var indexPaths:[IndexPath] = []
//        for i in oldLastIndex+1...newLastIndex {
//            indexPaths.append(IndexPath(row: i, section: 0))
//        }
//
//        let bottomOffset = self.contentSize.height - self.contentOffset.y;
//        CATransaction.begin()
//        CATransaction.setDisableActions(true)
//        self.performBatchUpdates {
//            self.insertItems(at: indexPaths)
//        } completion: { success in
//            self.contentOffset = CGPoint(x: 0, y: self.contentSize.height - bottomOffset)
//        }
//        CATransaction.commit()

    }
}

extension MoviesHorizontalCollection: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.model.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard self.model.count > indexPath.row,
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieHorizontalCell", for: indexPath) as? MovieHorizontalCell
        else { return UICollectionViewCell() }
        
        let movie = self.model[indexPath.row]
        cell.set(movie)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard self.model.count > indexPath.row else { return }
        self.movieDelegate?.moviesHorizontal(collection: self, didSelect: self.model[indexPath.row])
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if self.model.count != 0,
           (self.model.count - 1) == indexPath.row {
            self.movieDelegate?.moviesHorizontalNextPage(self)
        }
    }
}
