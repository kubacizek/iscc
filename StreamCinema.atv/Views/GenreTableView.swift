//
//  GenreTableView.swift
//  StreamCinema.atv
//
//  Created by SCC on 22/11/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import Combine

protocol GenreViewModelDelegate: class {
    func genreViewModel(_ viewModel:GenreViewModel, didSelect movie:SCCMovie)
    func genreViewModel(_ viewModel: GenreViewModel, indexPath cell: GenreTableCell) -> Int?
}


final class GenreViewModel {

    private var model:[Genere] {
        get {
            var generes = Genere.allTvShows
            if self.type == .movie {
                generes = Genere.allMovie
            }
            if let isDisabled = (UIApplication.shared.delegate as? AppDelegate)?.appData.isDisableAdult,
               isDisabled == true {
                generes = generes.filter { genere -> Bool in
                    if Genere.explicit.contains(genere) {
                        return false
                    }
                    return true
                }
            }
            return generes
        }
    }
    private var modelData: [Genere:SCCMovieResult] = [:]
    private var focusItems: [Int:Int] = [:]

    private let appData: AppData
    public var type: FilterType = .movie
    public weak var genreDelegate:GenreViewModelDelegate?

    let errorSubject: PassthroughSubject<Error, Never> = PassthroughSubject()
    private var cancelables: Set<AnyCancellable> = Set()

    init(appData: AppData) {
        self.appData = appData
    }

    public func getNumberOfRows(in section:Int) -> Int {
        return self.model.count
    }
    
    public func getCell(forRowAt indexPath:IndexPath, in tableView:UITableView) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "GenreTableCell") as? GenreTableCell {
            let genre = self.model[indexPath.row]
            let result = self.modelData[genre]
            cell.delegate = self
            cell.cell(for:genre, type: self.type ,model: result, indexPath: indexPath)
            cell.focusDelegate = self
            cell.selectionStyle = .none
            cell.focusStyle = .custom
            return cell
        }
        return UITableViewCell()
    }
    
    private func fetchData(for genre:Genere, page: Int, cell: GenreTableCell) {
        if let pagination = self.modelData[genre]?.pagination, pagination.page >= page {
            return
        }
        appData.scService
            .genre(with: genre, type: type, page: page)
            .assignError(to: errorSubject)
            .sink { [weak self] model in
                self?.update(data: model, genre: genre, page: page, cell: cell)
            }.store(in: &cancelables)
    }
    
    private func update(data:SCCMovieResult, genre:Genere, page: Int, cell: GenreTableCell) {
        if page == 1 {
            self.modelData[genre] = data
        } else {
            if let pagination = data.pagination {
                self.modelData[genre]?.data.append(contentsOf: data.data)
                self.modelData[genre]?.pagination = pagination
            }
        }
        if cell.genre == genre {
            cell.update(model: self.modelData[genre])
        }
        cell.collectionView.reloadData()
    }
}

extension GenreViewModel: GenreTableCellFocusDelegate {
    func focusForIndexPathGenreCell(_ cell: GenreTableCell) -> IndexPath? {
        guard let cellIndex = self.genreDelegate?.genreViewModel(self, indexPath: cell),
              let row = self.focusItems[cellIndex] else { return  nil}
        return IndexPath(row: row, section: 0)
    }
    
    func genreTableCell(_ cell: GenreTableCell, didFocus index: Int) {
        guard let cellIndex = self.genreDelegate?.genreViewModel(self, indexPath: cell) else { return }
        self.focusItems[cellIndex] = index
    }
}

extension GenreViewModel: GenreTableCellDelegate {
    func genreTableCell(_ cell: GenreTableCell, dataFor genre: Genere, page: Int) {
        self.fetchData(for: genre, page: page, cell: cell)
    }
    
    func genreTableCell(_ cell: GenreTableCell, didSelect movie: SCCMovie) {
        self.genreDelegate?.genreViewModel(self, didSelect: movie)
    }
}

protocol GenreTableDelegate: class {
    func castCollectionView(_ castView:GenreTableView, didSelect movie:SCCMovie)
}

final class GenreTableView: UITableView {

    static func create(appData: AppData, frame: CGRect, style: UITableView.Style) -> GenreTableView {
        let view = GenreTableView(frame: frame, style: style)
        view.appData = appData
        view.model =  GenreViewModel(appData: appData)
        view.configure()
        return view
    }

    public weak var genreDelegate:GenreTableDelegate?
    public var type: FilterType = .movie {
        didSet {
            self.model.type = self.type
        }
    }

    private var appData: AppData!
    private var model: GenreViewModel!

    let errorSubject: PassthroughSubject<Error, Never> = PassthroughSubject()
    private var cancelables: Set<AnyCancellable> = Set()
    
    private func configure() {
        self.model.genreDelegate = self
        self.delegate = self
        self.dataSource = self
        self.register(UINib(nibName: "GenreTableCell", bundle: nil), forCellReuseIdentifier: "GenreTableCell")
        self.rowHeight = UITableView.automaticDimension
        setupSubcriptions()
    }

    private func setupSubcriptions() {
        model
            .errorSubject
            .sink { (error) in
                self.errorSubject.send(error)
            }.store(in: &cancelables)
    }

}

extension GenreTableView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 640
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.model.getNumberOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.model.getCell(forRowAt: indexPath, in: tableView)
    }
    
    func tableView(_ tableView: UITableView, canFocusRowAt indexPath: IndexPath) -> Bool {
        return false
    }
}

extension GenreTableView: GenreViewModelDelegate {
    func genreViewModel(_ viewModel: GenreViewModel, didSelect movie: SCCMovie) {
        self.genreDelegate?.castCollectionView(self, didSelect: movie)
    }
    
    func genreViewModel(_ viewModel: GenreViewModel, indexPath cell: GenreTableCell) -> Int? {
        return self.indexPath(for: cell)?.row
    }
}
