//
//  PlayerBottomSeekBar.swift
//  StreamCinema.atv
//
//  Created by SCC on 29/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

enum BottomSeekAction {
    case play
    case stop
    case forward
    case backward
    case subttiles
    case audio
}

protocol PlayerBottomSeekDelegate {
    func playerBottomSeekBar(_ bar: PlayerBottomSeekBar, perform action:BottomSeekAction)
}

final class PlayerBottomSeekBar: UIView {
    let btnSize:CGFloat = 40.0
    let btnSpace:CGFloat = 25
    
    public var delegate: PlayerBottomSeekDelegate?
    public var isPlaying:Bool = false {
        willSet {
            if newValue {
                self.playButton.setFullImage(name: "pause", for: .normal)
                self.playButton.setFullImage(name: "pause.fill", for: .focused)
            } else {
                self.playButton.setFullImage(name: "play", for: .normal)
                self.playButton.setFullImage(name: "play.fill", for: .focused)
            }
        }
    }
    
    let playButton: UIButton = UIButton()
    let stopButtpn: UIButton = UIButton()
    let backwardButton: UIButton = UIButton()
    let forwardButton: UIButton = UIButton()
    let audioButton: UIButton = UIButton()
    let subttitlesButton: UIButton = UIButton()
    let progressBar: UIProgressView = UIProgressView(progressViewStyle: .default)
    let currentTime: UILabel = UILabel()
    let endTime: UILabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setupSubviews()
    }
    
    private func setActionsForButtons() {
        self.playButton.addTarget(self, action: #selector(play), for: .primaryActionTriggered)
        self.stopButtpn.addTarget(self, action: #selector(stop), for: .primaryActionTriggered)
        self.backwardButton.addTarget(self, action: #selector(backward), for: .primaryActionTriggered)
        self.forwardButton.addTarget(self, action: #selector(forward), for: .primaryActionTriggered)
        self.audioButton.addTarget(self, action: #selector(audio), for: .primaryActionTriggered)
        self.subttitlesButton.addTarget(self, action: #selector(subtitles), for: .primaryActionTriggered)
    }
    
    private func setupColorsAndIcons() {
        self.playButton.setFullImage(name: "play", for: .normal)
        self.stopButtpn.setFullImage(name:  "stop", for: .normal)
        self.backwardButton.setFullImage(name: "backward", for: .normal)
        self.forwardButton.setFullImage(name: "forward", for: .normal)
        self.audioButton.setFullImage(name: "waveform", for: .normal)
        self.subttitlesButton.setFullImage(name: "text.bubble", for: .normal)
        
        self.playButton.setFullImage(name: "play.fill", for: .focused)
        self.stopButtpn.setFullImage(name:  "stop.fill", for: .focused)
        self.backwardButton.setFullImage(name: "backward.fill", for: .focused)
        self.forwardButton.setFullImage(name: "forward.fill", for: .focused)
        self.audioButton.setFullImage(name: "waveform.circle.fill", for: .focused)
        self.subttitlesButton.setFullImage(name: "text.bubble.fill", for: .focused)
        
        self.playButton.imageView?.image?.withRenderingMode(.alwaysTemplate)
        self.stopButtpn.imageView?.image?.withRenderingMode(.alwaysTemplate)
        self.backwardButton.imageView?.image?.withRenderingMode(.alwaysTemplate)
        self.forwardButton.imageView?.image?.withRenderingMode(.alwaysTemplate)
        self.audioButton.imageView?.image?.withRenderingMode(.alwaysTemplate)
        self.subttitlesButton.imageView?.image?.withRenderingMode(.alwaysTemplate)
        
        self.playButton.tintColor = .white
        self.stopButtpn.tintColor = .white
        self.backwardButton.tintColor = .white
        self.forwardButton.tintColor = .white
        self.audioButton.tintColor = .white
        self.subttitlesButton.tintColor = .white
        
        self.currentTime.adjustsFontSizeToFitWidth = true
        self.currentTime.font = UIFont.systemFont(ofSize: 15)
        self.currentTime.textColor = .white
        self.endTime.adjustsFontSizeToFitWidth = true
        self.endTime.font = UIFont.systemFont(ofSize: 15)
        self.endTime.textColor = .white
    }
    
    private func setupSubviews() {
        self.addSubview(self.playButton)
        self.addSubview(self.stopButtpn)
        self.addSubview(self.backwardButton)
        self.addSubview(self.forwardButton)
        self.addSubview(self.audioButton)
        self.addSubview(self.subttitlesButton)
        self.addSubview(self.progressBar)
        self.addSubview(self.currentTime)
        self.addSubview(self.endTime)
        
        self.setupColorsAndIcons()
        self.setActionsForButtons()

        self.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        self.playButton.translatesAutoresizingMaskIntoConstraints = false
        self.stopButtpn.translatesAutoresizingMaskIntoConstraints = false
        self.backwardButton.translatesAutoresizingMaskIntoConstraints = false
        self.forwardButton.translatesAutoresizingMaskIntoConstraints = false
        self.audioButton.translatesAutoresizingMaskIntoConstraints = false
        self.subttitlesButton.translatesAutoresizingMaskIntoConstraints = false
        self.progressBar.translatesAutoresizingMaskIntoConstraints = false
        self.currentTime.translatesAutoresizingMaskIntoConstraints = false
        self.endTime.translatesAutoresizingMaskIntoConstraints = false
        
        self.playButton.heightAnchor.constraint(equalToConstant: btnSize).isActive = true
        self.playButton.widthAnchor.constraint(equalToConstant: btnSize).isActive = true
        self.stopButtpn.heightAnchor.constraint(equalToConstant: btnSize).isActive = true
        self.stopButtpn.widthAnchor.constraint(equalToConstant: btnSize).isActive = true
        self.backwardButton.heightAnchor.constraint(equalToConstant: btnSize).isActive = true
        self.backwardButton.widthAnchor.constraint(equalToConstant: btnSize).isActive = true
        self.forwardButton.heightAnchor.constraint(equalToConstant: btnSize).isActive = true
        self.forwardButton.widthAnchor.constraint(equalToConstant: btnSize).isActive = true
        self.audioButton.heightAnchor.constraint(equalToConstant: btnSize).isActive = true
        self.audioButton.widthAnchor.constraint(equalToConstant: btnSize).isActive = true
        self.subttitlesButton.heightAnchor.constraint(equalToConstant: btnSize).isActive = true
        self.subttitlesButton.widthAnchor.constraint(equalToConstant: btnSize).isActive = true

        self.playButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
        self.playButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -20 ).isActive = true
        
        self.stopButtpn.centerYAnchor.constraint(equalTo: self.playButton.centerYAnchor).isActive = true
        self.stopButtpn.leftAnchor.constraint(equalTo: self.playButton.rightAnchor, constant: btnSpace).isActive = true
        
        self.backwardButton.centerYAnchor.constraint(equalTo: self.playButton.centerYAnchor).isActive = true
        self.backwardButton.leftAnchor.constraint(equalTo: self.stopButtpn.rightAnchor, constant: btnSpace).isActive = true
        
        self.forwardButton.centerYAnchor.constraint(equalTo: self.playButton.centerYAnchor).isActive = true
        self.forwardButton.leftAnchor.constraint(equalTo: self.backwardButton.rightAnchor, constant: btnSpace).isActive = true
        
        self.audioButton.centerYAnchor.constraint(equalTo: self.playButton.centerYAnchor).isActive = true
        self.audioButton.leftAnchor.constraint(equalTo: self.forwardButton.rightAnchor, constant: btnSpace).isActive = true
        
        self.subttitlesButton.centerYAnchor.constraint(equalTo: self.playButton.centerYAnchor).isActive = true
        self.subttitlesButton.leftAnchor.constraint(equalTo: self.audioButton.rightAnchor, constant: btnSpace).isActive = true
        
        
        self.currentTime.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
        self.currentTime.heightAnchor.constraint(equalToConstant: 20).isActive = true
        self.currentTime.widthAnchor.constraint(equalToConstant: 50).isActive = true
        self.currentTime.bottomAnchor.constraint(equalTo: self.playButton.topAnchor, constant: -10).isActive = true
        self.currentTime.topAnchor.constraint(equalTo: self.topAnchor, constant: 20).isActive = true
        
        self.progressBar.leftAnchor.constraint(equalTo: self.currentTime.rightAnchor, constant: 20).isActive = true
        self.progressBar.centerYAnchor.constraint(equalTo: self.currentTime.centerYAnchor, constant: 0).isActive = true
        self.progressBar.rightAnchor.constraint(equalTo: self.endTime.leftAnchor, constant: -20).isActive = true
        self.progressBar.heightAnchor.constraint(equalToConstant: 10).isActive = true
        
        self.endTime.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
        self.endTime.heightAnchor.constraint(equalToConstant: 20).isActive = true
        self.endTime.widthAnchor.constraint(equalToConstant: 50).isActive = true
        self.endTime.centerYAnchor.constraint(equalTo: self.currentTime.centerYAnchor, constant: 0).isActive = true
        
    }
    
    //MARK: - update view
    public func set(duration:String) {
        self.endTime.text = duration
    }
    
    public func setProgress(_ progress: Float) {
        if self.isHidden {
            self.progressBar.setProgress(progress, animated: false)
        } else {
            self.progressBar.setProgress(progress, animated: true)
        }
        self.currentTime.text = "\(progress)"
    }
    
    //MARK: - actions
    @objc private func play() {
        self.delegate?.playerBottomSeekBar(self, perform: .play)
    }
    
    @objc private func stop() {
        self.delegate?.playerBottomSeekBar(self, perform: .stop)
    }
    
    @objc private func forward() {
        self.delegate?.playerBottomSeekBar(self, perform: .forward)
    }
    
    @objc private func backward() {
        self.delegate?.playerBottomSeekBar(self, perform: .backward)
    }
    
    @objc private func subtitles() {
        self.delegate?.playerBottomSeekBar(self, perform: .subttiles)
    }
    
    @objc private func audio() {
        self.delegate?.playerBottomSeekBar(self, perform: .audio)
    }
}
