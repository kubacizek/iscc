//
//  UIViewController+Alert.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 22.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit

extension UIViewController {

    func presentErrorAlert(error: Error) {
        presentMessage(title: "Error occured", text: error.localizedDescription)
    }

    func presentMessage(title: String, message: String, onClosed: (() -> Void)?) {
        let action = UIAlertAction(title: String(localized: .buttonOK), style: .default) { _ in
            onClosed?()
        }
        presentAlert(title: title, message: message, actions: [action])
    }


    func presentErrorAlert(error: RequestErrors, onClosed: (() -> Void)? = nil) {
        let alert = UIAlertController(title: String(localized: .error), message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: String(localized: .buttonOK), style: .cancel, handler: { _ in
                onClosed?()
        }))
        alert.setupSCCStyle()
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }

    func presentMessage(title: String?, text: String) {
        let alert = UIAlertController(title: title, message: text, preferredStyle: .alert)
        let ok = UIAlertAction(title: String(localized: .buttonOK), style: .default, handler: nil)
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
    }

    func presentAlert(title: String, message: String, actions: [UIAlertAction]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        actions.forEach { alert.addAction($0) }
        present(alert, animated: true, completion: nil)
    }

    func presentConfirmationAlert(title: String?, message: String?, onConfirmed: @escaping () -> Void, onCancel: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let confirmStr = String(localized: .buttonOK)
        let cancelStr = String(localized: .buttonCancel)
        alert.addAction(UIAlertAction(title: confirmStr, style: .default, handler: { _ in onConfirmed() }))
        alert.addAction(UIAlertAction(title: cancelStr, style: .cancel, handler: { _ in onCancel?() }))
        present(alert, animated: true, completion: nil)
    }

    func presentAlertActionSheet(title: String?, message: String?, actions: [UIAlertAction], completion: (() -> Void)?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        actions.forEach({ action in
            alertController.addAction(action)
            if let title = action.title, title.contains("✓  ") {
                alertController.preferredAction = action
            }
        })
        let cancelAction = UIAlertAction(title: String(localized: .buttonCancel), style: .cancel) { [weak self] _ in
        }
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: completion)
    }

}

extension UIViewController {

    /// Method handle only common erroir. Otherwise it returns it.
  @objc open func handleCommonError(_ error: Error) -> Error? {

    if let commonError = error as? CommonErrors {
        presentMessage(title: "Common Error", message: commonError.localizedDescription) {
            // TODO: Does we want to show some login screen or what?
            // eg. NotificationCenter.default.post(name: .loginExpired, object: nil)
        }
        return nil
    } else {
        return error
    }
  }
}

