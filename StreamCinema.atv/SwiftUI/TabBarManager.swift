//
//  TabBarManager.swift
//  iSCC-iOS
//
//  Created by Martin Matějka on 19.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import Foundation
import Combine
import TraktKit


/// Defined for TabBarView (SwiftUI)
protocol TabBarViewDelegate {

    var selectedItemPublisher: AnyPublisher<TabBarItem, Never> { get }
    var updateFocusEventPublisher: AnyPublisher<Void, Never> { get }

    var mainItems: [TabBarItem] { get }
    var bottomItems: [TabBarItem] { get }

    var selectedItem: TabBarItem { get }
    func select(item: TabBarItem)
}

/// Defined for UITabBarViewController (UIKit)
protocol TabBarViewControllerDelegate {

    /// For these items are created UIViewControllers
    var allItems: [TabBarItem] { get }

    var presentScreenPublisher: AnyPublisher<Int, Never> { get }
    var popToRootEventPublisher: AnyPublisher<Bool, Never> { get }

}


/// TabBarManager works as communicator between the UITabBarViewController and TabBarView(SwiftUI)
/// Each of them has own defined protocol which informations needs or controls him. In the future can be
/// this object devided on two separete objects for each protocol.
class TabBarManager {

    // MARK: - Properties for TabBarViewControllerDelegate
    private let popToRootEventSubject: PassthroughSubject<Bool, Never> =
        PassthroughSubject()

    // MARK: - Properties for TabBarViewDelegate
    @Input private(set) var selectedItem: TabBarItem
    private let updateFocusEventSubject: PassthroughSubject<Void, Never> = PassthroughSubject()


    // MARK: - Other properties
    private let traktManager: TraktManager

    init(traktManager: TraktManager) {
        self.selectedItem = .movies
        self.traktManager = traktManager
    }

    // MARK: - Public methods
    func needsUpdateFocus() {
        updateFocusEventSubject.send(())
    }

    // MARK: - Private methods
    private func popToRoot(animanted: Bool = true) {
        popToRootEventSubject.send(animanted)
    }

    private func screenIndex(for item: TabBarItem) -> Int {
        let isSigneInTrakt = traktManager.isSignedIn
        switch item {
        case .fullTextSearch: return 0
        case .movies: return 1
        case .tvShows: return 2
        case .concerts: return 3
        case .trakt: return isSigneInTrakt ? 4 : -1
        case .search: return -1
        case .tvProgram: return -1
        case .tv: return isSigneInTrakt ? 5 : 4
        case .settings: return isSigneInTrakt ? 6 : 5
        }
    }

}


extension TabBarManager: TabBarViewDelegate {

    var updateFocusEventPublisher: AnyPublisher<Void, Never> {
        updateFocusEventSubject.eraseToAnyPublisher()
    }

    var mainItems: [TabBarItem] {
        var items: [TabBarItem] = [
            .fullTextSearch,
            .movies,
            .tvShows,
            .concerts
        ]
        if traktManager.isSignedIn {
            items.append(.trakt)
        }
        items.append(.tv)
        return items
    }

    var bottomItems: [TabBarItem] {
        [.settings]
    }

    var selectedItemPublisher: AnyPublisher<TabBarItem, Never> { $selectedItem }

    func select(item: TabBarItem) {
        if selectedItem == item {
            popToRoot(animanted: true)
        } else {
            selectedItem = item
        }
    }

}

extension TabBarManager: TabBarViewControllerDelegate {


    var allItems: [TabBarItem] {
        mainItems + bottomItems
    }

    var presentScreenPublisher: AnyPublisher<Int, Never> { $selectedItem.map{ self.screenIndex(for: $0) }.eraseToAnyPublisher() }
    var popToRootEventPublisher: AnyPublisher<Bool, Never> {
        popToRootEventSubject.eraseToAnyPublisher()
    }


}
