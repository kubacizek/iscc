//
//  MoviewAndShowViewModel.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 21.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit
import Combine
import TraktKit

final class MovieAndShowViewModel {

    enum MovieAndShowError: Error {
        case noNextEpisodInformations
    }

    var movieData: SCCMovie

    var modelData: MovieModel = MovieModel()
    private let appData: AppData

    // Callbacks for presenter
    var onReloadEpisodes: (([SCCMovie],Int?) -> Void)?
    var onDidUpdateModel: (() -> Void)?

    let errorSubject: PassthroughSubject<Error, Never> = PassthroughSubject()
    private var cancelables: Set<AnyCancellable> = Set()
    
    private var sesionPage: Pagination?
    private var episodePage: Pagination?

    init(movieData: SCCMovie, appData: AppData) {
        self.movieData = movieData
        self.appData = appData

        self.modelData.rootMovie = movieData
        self.modelData.casts = movieData.cast ?? []

        self.getRelated()
        self.getSessionInfo()
    }

    // MARK: - Public methods

    func didSelectSeason(ids: IDs?) -> AnyPublisher<Void, Never> {
        guard let season = modelData.seasons.first(where: { model -> Bool in return model.ids == ids })?.season else {
            return Empty().eraseToAnyPublisher()
        }
        return select(season: season, episode: 1)
            .handleEvents(receiveOutput: { [weak self] _ in
                self?.modelData.selected.season = season
            })
            .eraseToAnyPublisher()
    }

    func didSelectEpisode(ids: IDs?) {
        guard let movieItem = self.modelData.episodes.first(where: { model -> Bool in return model.ids == ids }) else { return }
        if let episodeNumber = movieItem.episode {
            self.modelData.selected.episode = episodeNumber
        }
    }
    

    func select(season: Int, episode: Int) -> AnyPublisher<Void, Never> {
        guard let seasonIdnex =  self.modelData.seasons.firstIndex(where: { $0.season == season }),
              seasonIdnex < self.modelData.seasons.count,
              let data = self.modelData.seasons[safe: seasonIdnex]
        else {
            return Just(()).eraseToAnyPublisher()
        }

        let currentSeason = self.modelData.selected.season
        self.modelData.selected = Selected(season: season, episode: episode)

        // TODO: We should hanled maybe this situation.
        guard let seasonID = data.ids?.sccID else { return Just(()).eraseToAnyPublisher() }

        if currentSeason != season {
            return getSessiondEpisodes(seasonID, type: data.traktType.media, showID: data.rootIDs, season: data.seasonIDs)
                .eraseToAnyPublisher()
        } else if self.modelData.episodes.count == 0 {
            return getSessiondEpisodes(seasonID, type: data.traktType.media, showID: data.rootIDs, season: data.seasonIDs)
                .eraseToAnyPublisher()
        } else {
            return Just(()).eraseToAnyPublisher()
        }
    }
    
    func removeFromAllWatchlist() {
        self.movieData.removeFromSccWatchList()
        guard let traktId = self.movieData.rootIDs?.trakt else { return }
        var movieSynch:[SyncId]? = nil
        var showSynch:[SyncId]? = nil
        if self.movieData.traktType == .movie {
            movieSynch = [SyncId(trakt: traktId)]
        }
        else if self.movieData.traktType == .tvshow {
            showSynch = [SyncId(trakt: traktId)]
        }
        
        _ = try? appData.traktManager?.removeFromWatchlist(movies: movieSynch, shows: showSynch, seasons: nil, episodes: nil, completion: { result in
            switch result {
            case .success(object: let object):
                break
            case .error(error: let error):
                break
            }
        })
    }
    
    func addToAllWatchList() {
        self.movieData.date = Date()
        self.movieData.addToSccWatchList()
        guard let traktId = self.movieData.rootIDs?.trakt else { return }
        var movieSynch:[SyncId]? = nil
        var showSynch:[SyncId]? = nil
        if self.movieData.traktType == .movie {
            movieSynch = [SyncId(trakt: traktId)]
        }
        else if self.movieData.traktType == .tvshow {
            showSynch = [SyncId(trakt: traktId)]
        }
        
        _ = try? appData.traktManager?.addToWatchlist(movies: movieSynch, shows: showSynch, seasons: nil, episodes: nil) { result in
            switch result {
            case .success(object: let object):
                break
            case .error(error: let error):
                break
            }
        }
    }
    
    // MARK: - pagining methods
    func fetchNextPageIfExists(for type:MovieViewCells) {
        if self.movieData.traktType == .tvshow {
            if let currentSeason = self.modelData.seasons[safe: self.modelData.selected.season],
               let currentSeasonSCCid = currentSeason.ids?.sccID {
                if type == .episodes, let page = self.episodePage, page.limit > page.page + 1 {
                   _ = getSessiondEpisodes(currentSeasonSCCid, type: currentSeason.traktType.media, showID: self.movieData.rootIDs, season: currentSeason.ids, page: page.page + 1)
                }
                else if type == .seasons, let page = self.sesionPage, page.limit > page.page + 1 {
                    self.getSessionInfo(page: page.page + 1)
                }
            } else if type == .episodes, let page = self.episodePage, page.limit > page.page + 1 {
                // if not exist session only episodes
                self.getSessionInfo(page: page.page + 1)
            }
        }
    }

    // MARK: - Private methods

    private func getSessionInfo(page:Int = 1) {
        guard movieData.traktType.media == .tvshow,
              let showId = movieData.ids?.sccID else { return }

        appData.scService
            .getSeries(for: showId, showID: modelData.rootMovie?.rootIDs, seasonID: nil, page: page)
            .assignError(to: errorSubject)
            .sink { filterResult in
                if let type = filterResult.data.first?.traktType,
                   type == .episode {
                    self.modelData.episodes = filterResult.data
                    if page == 1 {
                        self.modelData.episodes = filterResult.data
                    } else {
                        self.modelData.episodes.append(contentsOf: filterResult.data)
                    }
                    self.episodePage = filterResult.pagination
                    self.onReloadEpisodes?(filterResult.data,filterResult.pagination?.page)
                } else {
                    if page == 1 {
                        self.modelData.seasons = filterResult.data
                    } else {
                        self.modelData.seasons.append(contentsOf: filterResult.data)
                    }
                    self.sesionPage = filterResult.pagination
                    self.searchWatchedAndUpdateEpisodesView()
                }
            }.store(in: &cancelables)
    }

    private func searchWatchedAndUpdateEpisodesView() { //todo odstranit stary pristup ku historii
        let lastPayedEpisode = WatchedWrapper.lastPlayedEpisode(for: self.movieData.ids)
        let lastEpisodeNumber = lastPayedEpisode?.episode

        if lastPayedEpisode?.season == nil,
           let lastEpisodeNumber = lastEpisodeNumber{
            self.select(season: 1, episode: lastEpisodeNumber)
                .sink { _ in
                    // SOME completion ??
                }.store(in: &cancelables)
        }

        else if let seasonNumber = lastPayedEpisode?.season,
           let episodeNumber = lastPayedEpisode?.episode {
            if let lastPayedEpisode = lastPayedEpisode,
               lastPayedEpisode.state == .done {
                getNext(episode: episodeNumber, season: seasonNumber)
                    .sink { isExitst in
                        if !isExitst {
                            self.getNext(season: seasonNumber)
                        }
                    }.store(in: &cancelables)
                return
            }
            self.select(season: seasonNumber, episode: episodeNumber)
                .sink { _ in
                    // SOME completion ??
                }.store(in: &cancelables)
        }
        else {
            self.select(season: 1, episode: 1)
                .sink { _ in
                    // SOME completion ??
                }.store(in: &cancelables)
        }
    }

    private func getNext(season:Int) {

        guard let root_parent = modelData.rootMovie?.ids?.sccID else { return }

        appData.scService
            .getNext(episode: "1", season: "\(season + 1)", root_parent: root_parent)
            .assignError(to: errorSubject)
            .flatMap { filterResult -> AnyPublisher<Void, Never> in
                guard let nextEpisode = filterResult.data.first,
                      let episodeNumber = nextEpisode.episode,
                      let seasonNumber = nextEpisode.season else {
                    return Empty().eraseToAnyPublisher() // Do we want here some error ?
                }
                return self.select(season: seasonNumber, episode: episodeNumber)
            }
            .sink { _ in
                Log.write("[MOVIE-SHOW]: Succesfully got and selected next SEASON")
            }.store(in: &cancelables)
    }

    private func getNext(episode: Int, season: Int) -> AnyPublisher<Bool, Never> {
        guard let root_parent = self.modelData.rootMovie?.ids?.sccID else { return Just(false).eraseToAnyPublisher() }


        return appData.scService
            .getNext(episode: "\(episode + 1)", season: "\(season)", root_parent: root_parent)
            .map { filterResult -> (Int, Int)? in
                guard let nextEpisode = filterResult.data.first,
                      let episodeNumber = nextEpisode.episode,
                      let seasonNumber = nextEpisode.season else { return nil }
                return (seasonNumber, episodeNumber)
            }
            .flatMap({ maybeNextEpisodInfo -> AnyPublisher<Void, Error> in
                guard let nextEpisodInfo = maybeNextEpisodInfo else {
                    return Fail(error: MovieAndShowError.noNextEpisodInformations).eraseToAnyPublisher()
                }
                return self.select(season: nextEpisodInfo.0, episode: nextEpisodInfo.1)
                    .setFailureType(to: Error.self)
                    .eraseToAnyPublisher()
            })
            .map { _ in return true }
            .replaceError(with: false)
            .eraseToAnyPublisher()
    }


    private func getSessiondEpisodes(_ seasonID: String, type:MediaType?, showID:IDs?, season: IDs?, page:Int = 1) -> AnyPublisher<Void, Never> {
        return appData.scService.getSeries(for: seasonID, showID: showID, seasonID: season, page: page)
            .assignError(to: errorSubject)
            .handleEvents(receiveOutput: { filterResult in
                self.episodePage = filterResult.pagination
                if page == 1 {
                    self.modelData.episodes = filterResult.data
                } else {
                    self.modelData.episodes.append(contentsOf: filterResult.data)
                }
                self.onReloadEpisodes?(filterResult.data,filterResult.pagination?.page)
            })
            .map { _ in () }
            .eraseToAnyPublisher()
    }


    private func getRelated() {
        guard let csfdID = movieData.ids?.csfd else { return }
        let csfdParser = CSFD()
        csfdParser.getRelated(with: "\(csfdID)", isTvShow: movieData.traktType.media == .tvshow) { result in
            switch result {
            case .success(let csfdIds):
                if csfdIds.count > 0 {
                    self.getRelatedMovie(csfdDs: csfdIds)
                }
            case .failure(let err):
                Log.write("get CSFD Related \(err)")
            }
        }
    }

    private func getRelatedMovie(csfdDs: [String]) {
        appData.scService
            .getCsfdMedia(for: csfdDs, type: .all, page: 1, sort: .news)
            .assignError(to: errorSubject)
            .map { data in
                return data.data.sorted { first, second in
                    if let firstY = first.year,
                       let rightY = second.year {
                        return firstY < rightY
                    }
                    return false
                }
            }.sink { sortedData in
                self.modelData.releatedMovie = sortedData
                self.onDidUpdateModel?()
            }
            .store(in: &cancelables)

    }

}


