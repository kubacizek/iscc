//
//  MovieAndShowViewController.swift
//  StreamCinema.atv
//
//  Created by SCC on 20/12/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import Kingfisher
import AVKit
import XCDYouTubeKit
import Combine

protocol MovieViewControllerDelegate: class {
    func presentCastDetailScreen(for cast: Cast)
    func presentMovieDetailScreen(for relatedMovie: SCCMovie)
    func presentVideoPlayer(model: MovieModel, isContinue: Bool)
}

final class MovieAndShowViewControllera: UIViewController {

    static func create(viewModel: MovieAndShowViewModel) -> MovieAndShowViewControllera {
        let vc = MovieAndShowViewControllera()
        vc.viewModel = viewModel
        return vc
    }

    private var movieTableView: MovieView?
    private var mivieView: UIView = UIView()
    private var imageView: UIImageView = UIImageView()
    private var titleView: TitleView = TitleView(frame: .zero)
    private var dimmingView: MovieGradientView = MovieGradientView()
    private var playerTrailer: AVPlayer?
    
    weak var delegate: MovieViewControllerDelegate?
    private var viewModel: MovieAndShowViewModel!
    private var cancelables: Set<AnyCancellable> = Set()


    // MARK: for presenting

    // MARK: - Life cycle medhotds

    override func viewDidLoad() {
        super.viewDidLoad()
        self.overrideUserInterfaceStyle = .dark
        setupViewModel()
        setupViews()
        setupBackgroundImage()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.stopPlayingTrailer()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.playTrailer()
    }

    // MARK: - Setups methods

    private func setupViews() {
        self.view.insetsLayoutMarginsFromSafeArea = false

        self.view.addSubview(self.imageView)
        self.imageView.translatesAutoresizingMaskIntoConstraints = false

        self.imageView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.imageView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        self.imageView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.imageView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true

        self.view.addSubview(self.mivieView)
        self.mivieView.translatesAutoresizingMaskIntoConstraints = false

        self.mivieView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.mivieView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        self.mivieView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.mivieView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true

        self.view.addSubview(self.titleView)
        self.titleView.translatesAutoresizingMaskIntoConstraints = false
        self.titleView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 32).isActive = true
        self.titleView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 32).isActive = true
        self.titleView.widthAnchor.constraint(equalToConstant: 300).isActive = true
        self.titleView.heightAnchor.constraint(equalToConstant: 150).isActive = true

        self.view.addSubview(self.dimmingView)
        self.dimmingView.translatesAutoresizingMaskIntoConstraints = false

        self.dimmingView.heightAnchor.constraint(equalToConstant: 470).isActive = true
        self.dimmingView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        self.dimmingView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.dimmingView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true

        self.movieTableView = MovieView(frame: .zero, style: .plain)
        self.movieTableView?.model = viewModel.modelData
        self.movieTableView?.movieDelegate = self
        self.movieTableView?.contentInset = .zero

        self.view.addSubview(self.movieTableView!)
        self.movieTableView?.translatesAutoresizingMaskIntoConstraints = false

        self.movieTableView?.heightAnchor.constraint(equalToConstant: 450).isActive = true
        self.movieTableView?.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        self.movieTableView?.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.movieTableView?.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
    }

    private func setupBackgroundImage() {
        if self.imageView.image == nil,
           let fanArt = viewModel.modelData.rootMovie?.fanart,
           !fanArt.absoluteString.contains("null") {
            self.imageView.kf.indicatorType = .none
            self.imageView.setCashedImageS(url: fanArt, type: .fanart)
        } else {
            self.imageView.image = UIImage(named: "fanart")
            viewModel.modelData.rootMovie?.cellBacgroundImage(completition: { url in
                if let url = url {
                    self.imageView.kf.indicatorType = .none
                    self.imageView.setCashedImageS(url: url, type: .fanart)
                }
            })
        }

        if let movieTitle = viewModel.modelData.rootMovie?.title {
            self.titleView.set(title: movieTitle, image: viewModel.modelData.rootMovie?.logo)
        }
    }

    private func setupViewModel() {
        viewModel.onReloadEpisodes = { [weak self] data, page in
            guard let number = page else { return }
            self?.reloadEpisode(data: data, page:number)
        }

        viewModel.onDidUpdateModel = { [weak self] in
            DispatchQueue.main.async {
                self?.updateTableModel()
                self?.reloadData()
            }
        }

        viewModel.errorSubject
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (error) in
                guard let otherError = self?.handleCommonError(error) else { return }
                self?.presentErrorAlert(error: otherError)
            }.store(in: &cancelables)
    }

    // MARK: - Private methods

    private func updateTableModel() {
        self.movieTableView?.model = self.viewModel.modelData
    }


    // MARK: - Public methods
    private func update(fanArt: URL) {
        self.imageView.kf.indicatorType = .none
        self.imageView.setCashedImageS(url: fanArt, type: .emptyLoading)
    }

    // MARK: - Private methods
    private func playTrailer() {
        guard let url = viewModel.modelData.rootMovie?.trailer else { return }
        if let range = url.range(of: "=") {
            let youtubeID = String(url[range.upperBound...])
            self.playYoutube(with: youtubeID)
        }
    }

    private func playYoutube(with youtubeID:String) {
        XCDYouTubeClient.default().getVideoWithIdentifier(youtubeID) { [weak self] (video: XCDYouTubeVideo?, error: Error?) in
           guard let self = self,
                 let video = video,
                 let videoURL = video.streamURL
           else { return }
            self.addVideoPlayer(videoURL)
        }
    }

    public func addVideoPlayer(_ videoUrl: URL) {
        self.stopPlayingTrailer()
        self.playerTrailer = AVPlayer(url: videoUrl)
        let layer: AVPlayerLayer = AVPlayerLayer(player: self.playerTrailer)
        layer.backgroundColor = UIColor.clear.cgColor
        layer.frame = view.bounds
        layer.videoGravity = .resizeAspectFill
        self.mivieView.layer.sublayers?
            .filter { $0 is AVPlayerLayer }
            .forEach { $0.removeFromSuperlayer() }
        self.mivieView.layer.addSublayer(layer)
        self.playerTrailer?.play()
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.playerTrailer?.currentItem, queue: .main) { [weak self] _ in
            guard let self = self else { return }
            self.playerTrailer?.seek(to: CMTime.zero)
            for layer in self.mivieView.layer.sublayers ?? [] {
                if layer.isKind(of: AVPlayerLayer.self) {
                    layer.removeFromSuperlayer()
                }
            }
        }
    }
    
    public func reloadEpisode(data: [SCCMovie], page: Int) {
        guard let cells = self.movieTableView?.visibleCells else { return }
        var isNeedInsertEpisodeCell = true
        cells
            .compactMap { $0 as? SeasonsCell }
            .forEach { seasonCell in
                guard let type = seasonCell.type, type == .episodes else { return }
                if page == 1 {
                    seasonCell.model = data
                } else {
                    seasonCell.model.append(contentsOf: data)
                }
                isNeedInsertEpisodeCell = false
            }
        if isNeedInsertEpisodeCell {
            self.updateTableModel()
        }
    }
    
    public func reloadData() {
        self.movieTableView?.reloadData()
    }
    
    public func stopPlayingTrailer() {
        guard self.playerTrailer != nil else { return }
        self.playerTrailer?.pause()
        self.playerTrailer?.seek(to: CMTime.zero)
        for layer in self.mivieView.layer.sublayers ?? [] {
            if layer.isKind(of: AVPlayerLayer.self) {
                layer.removeFromSuperlayer()
            }
        }
        self.playerTrailer = nil
    }
}

extension MovieAndShowViewControllera : MovieViewDelegate {
    
    func movieView(_ movieView: MovieView, for nextPage: MovieViewCells?) {
        guard let type = nextPage else { return }
        self.viewModel?.fetchNextPageIfExists(for: type)
    }

    func movieView(_ movieView: MovieView, didPerfom action: MovieViewAction) {
        stopPlayingTrailer()
        if action == .playMovie {
            viewModel.select(season: 1, episode: 1)
                .sink { [weak self] _ in
                    guard let `self` = self else { return }
                    self.delegate?.presentVideoPlayer(model: self.viewModel.modelData, isContinue: false)
                }.store(in: &cancelables)

        } else if action == .continueMovie {
            if let lastPlayed = WatchedWrapper.lastPlayedEpisode(for: self.viewModel.modelData.rootMovie?.ids),
               let episode = lastPlayed.episode,
               let season = lastPlayed.season {
                viewModel.select(season: season, episode: episode)
                    .sink { [weak self] _ in
                        guard let `self` = self else { return }
                        self.delegate?.presentVideoPlayer(model: self.viewModel.modelData, isContinue: true)
                    }.store(in: &cancelables)
            } else {
                self.delegate?.presentVideoPlayer(model: self.viewModel.modelData, isContinue: true)
            }
        } else  if action == .addToWatchList {
            self.viewModel.addToAllWatchList()
        } else  if action == .removeFromWatchList {
            self.viewModel.removeFromAllWatchlist()
        }
    }

    func movieView(_ movieView: MovieView, showDetailFor movie: SCCMovie) {
        self.delegate?.presentMovieDetailScreen(for: movie)
    }

    func movieView(_ movieView: MovieView, showDetailFor cast: Cast) {
        self.delegate?.presentCastDetailScreen(for: cast)
    }

    func movieView(_ movieView: MovieView, focused item: SCCMovie) {
        guard let fanArt = item.fanart else { return }
        update(fanArt: fanArt)
    }

    func movieView(_ movieView: MovieView, didSelect item: SCCMovie) {

        switch item.traktType {
        case .season:
            viewModel.didSelectSeason(ids: item.ids)
                .sink { _ in
                    guard let ids = item.ids else { return }
                    Log.write("[MOVIE-DETAIL]: Season selected \(ids) and reloaded her episodes")
                }.store(in: &cancelables)
        case .episode:
            viewModel.didSelectEpisode(ids: item.ids)
            stopPlayingTrailer()
            self.delegate?.presentVideoPlayer(model: self.viewModel.modelData, isContinue: false)
        default: break
        }
    }
}

final class TitleView: UIView {
    private var titleLabel: UILabel = UILabel()
    private var titleImage: UIImageView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.configureView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func set(title:String?, image url: URL?) {
        self.titleImage.kf.indicatorType = .none
        self.titleLabel.text = title
        if let logo = url {
            self.titleImage.setCashedImageS(url: logo, type: .emptyLoading)
            self.titleLabel.isHidden = true
        } else {
            self.titleLabel.isHidden = false
        }
    }
    
    private func configureView() {
        self.addSubview(self.titleLabel)
        self.addSubview(self.titleImage)
        
        self.titleImage.contentMode = .scaleAspectFit
        
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = false
        self.titleImage.translatesAutoresizingMaskIntoConstraints = false
        
        self.titleImage.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        self.titleImage.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        self.titleImage.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        self.titleImage.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        
        self.titleLabel.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        self.titleLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        self.titleLabel.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        self.titleLabel.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        
    }
}

final class MovieGradientView: UIView {
    var topColor: UIColor = UIColor.black.withAlphaComponent(0)
    var midlleColor: UIColor = UIColor.black.withAlphaComponent(0.65)
    var bottomColor: UIColor = UIColor.black.withAlphaComponent(0.85)

    override public class var layerClass: AnyClass {
        return CAGradientLayer.classForCoder()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        (layer as? CAGradientLayer)?.colors = [topColor.cgColor,
                                               midlleColor.cgColor,
                                               midlleColor.cgColor,
                                               midlleColor.cgColor,
                                               bottomColor.cgColor]
    }
}
