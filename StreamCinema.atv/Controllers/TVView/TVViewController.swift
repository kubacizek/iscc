//
//  TVViewController.swift
//  StreamCinema.atv
//
//  Created by SCC on 25/11/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import Combine


protocol TVViewControllerDelegate: class {
    func presentVideoPlayer(tvLink: URL, title: String?, logo: URL?, desc: String?)
}

final class TVViewController: UITableViewController {

    static func create(appData: AppData) -> TVViewController {
        let vc = TVViewController(style: .grouped)
        vc.appData = appData
        return vc
    }

    private var appData: AppData!
    private var model: [TV] = [] {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
            }
        }
    }

    private var errorSubject: PassthroughSubject<Error, Never> = PassthroughSubject()
    var tvDelegate: TVViewControllerDelegate?
    private var cancelables: Set<AnyCancellable> = Set()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: "TVStationCell", bundle: nil), forCellReuseIdentifier: "TVStationCell")
        self.tableView.remembersLastFocusedIndexPath = true
        setupMenuButtonToForceFocusOnTabBar()
        setupSubsriptions()
        fetchTVStations()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }


    private func setupSubsriptions() {
        errorSubject
            .receive(on: DispatchQueue.main)
            .sink {[weak self] (error) in
                guard let otherError = self?.handleCommonError(error) else { return }
                self?.presentErrorAlert(error: otherError)
            }.store(in: &cancelables)
    }
    
    private func fetchTVStations() {
        appData.isccService
            .getTVdata()
            .assignError(to: errorSubject)
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { (model) in
                self.model = model.data
            }).store(in: &cancelables)
    }
    
    private func loadEpg(for station:String, reload indexPath: IndexPath) {

        appData.isccService
            .getEPG(for: station)
            .assignError(to: errorSubject)
            .sink { (data) in
                let index = self.model.firstIndex { tv -> Bool in
                    tv.tvid == station
                }
                if let index = index {
                    self.model[index].epg = data
                    if self.model[index].isReloaded == false {
                        self.tableView.reloadRows(at: [indexPath], with: .none)
                        self.model[index].isReloaded = true
                    }
                }
            }.store(in: &cancelables)
    }

}

extension TVViewController {
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 158
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.model.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TVStationCell") as? TVStationCell else { return UITableViewCell() }
        let model = self.model[indexPath.row]
        if let tvlogo = model.tvlogo,
            let url = URL(string: tvlogo) {
            cell.stationPincon.setCashedImage(url: url, type: .emptyLoading)
        }
        if let epg = model.epg {
            cell.titleLabel.text = epg.currnet?.programName
            cell.nextTitle.text = epg.next?.programName
            if let start = epg.currnet?.getStart(),
               let end = epg.currnet?.getEnd() {
                cell.setProgress(start: start, stop: end, current: Date(), nextStart: epg.next?.getStart(), nextEnd: epg.next?.getEnd())
            }
        } else if let tvID = model.tvid {
            self.loadEpg(for: tvID, reload: indexPath)
        }
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = self.model[indexPath.row]
        if let urlStr = model.url,
           let url = URL(string: urlStr),
           let logo = model.tvlogo,
           let delegate = self.tvDelegate {
            delegate.presentVideoPlayer(tvLink: url,
                                                title: model.tvtitle,
                                                logo: URL(string: logo),
                                                desc: model.epg?.currnet?.desc)
        }
    }
}
