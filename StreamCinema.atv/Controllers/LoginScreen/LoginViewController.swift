//
//  TabBarController.swift
//  StreamCinema.atv
//
//  Created by SCC on 03/07/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import iSCCCore
import Combine

final class LoginViewController: UIViewController {

    static func create(viewModel: LoginViewModel) -> LoginViewController {
        let vc = UIStoryboard.main.instantiateViewController(identifier: "LoginVC") as! LoginViewController
        vc.viewModel = viewModel
        return vc
    }

    @IBOutlet var buttonLogin: UIButton!
    @IBOutlet var titleLabel: UILabel!
    private var alert:UIAlertController?

    private var viewModel: LoginViewModel!
    private var cancelables: Set<AnyCancellable> = Set()

    var onAuthenticated: (() -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
        self.setUpTapGesture()
        setupSubscriptions()
    }

    private func setupSubscriptions() {
        viewModel.errorSubject
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (error) in
                self?.presentErrorAlert(error: error)
            }.store(in: &cancelables)
    }

    @IBAction func buttonLoginPressed() {
        self.login()
    }
    
    private func setUpTapGesture() {
     #if os(tvOS)
       let tap = UITapGestureRecognizer()
       tap.allowedPressTypes = [NSNumber(value: UIPress.PressType.menu.rawValue)]
       tap.addTarget(self, action: #selector(LoginViewController.sendEndMenuTap))
       view.addGestureRecognizer(tap)
     #endif
   }
    
   @objc func sendEndMenuTap(_ gestureRecognizer: UITapGestureRecognizer) {
    #if os(tvOS)
     UIApplication.shared.pressesEnded([MenuUIPress(view: view)], with: UIPressesEvent())
    #endif
   }
    
    private func configureView() {
        self.buttonLogin.setTitle(String(localized: .loginButton), for: .normal)
        self.titleLabel.text = String(localized: .logonScreenTitle)
    }
    
    private func showNeedCredentials(message: String? = String(localized: .to_watch_the_content_you_need_vip)) {
        alert = UIAlertController(title: String(localized: .missing_credentials),
                                       message: message ,
                                       preferredStyle: .alert)
        alert?.addTextField(configurationHandler: { textField in
            textField.placeholder = String(localized: .username)
        })
        alert?.addTextField(configurationHandler: { textField in
            textField.isSecureTextEntry = true
            textField.placeholder = String(localized: .password)
        })
        
        let action = UIAlertAction(title: String(localized: .button_continue), style: .default) { [weak self] _ in
            guard let self = self else { return }
            let name = self.alert?.textFields?[0].text
            let pass = self.alert?.textFields?[1].text
            self.login(name: name, pass: pass)
        }
        
        alert?.addAction(action)
//        alert?.addAction(UIAlertAction(title: nil, style: .cancel, handler: nil))
        self.present(alert!, animated: true, completion: nil)
    }
    
    private func login(name: String? = nil, pass: String? = nil) {

        viewModel.appData.loginManager
            .connect(name: name, pass: pass)
            .sink { [weak self] completion in
                guard case .failure(let error) = completion else { return }
                if case .unknownUserLoginCredentials = error as? LoginManager.LoginManagerError {
                    self?.showNeedCredentials()
                } else {
                    self?.showNeedCredentials(message: String(localized: .bad_credentials))
                }
            } receiveValue: { [weak self] _ in
                if let alert = self?.alert {
                    alert.dismiss(animated: false, completion: {
                        self?.dismiss(animated: true, completion: { [weak self] in
                            self?.onAuthenticated?()
                        })
                    })
                    return
                }
            }.store(in: &cancelables)
    }
}
