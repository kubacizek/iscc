//
//  VLCPlayerViewController+AVManager.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 23.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit
import AVKit


//MARK: - AV Display Manager

enum DynamicRange: Int32 {
    case SDR // 0,1
    case HDR  // 2,3
    case HDR10 // 2,3,4
    case DolbyVision // 5!!
    case Unknown
}

public extension VLCPlayerViewController {

    private var displayManager: AVDisplayManager? {
        if let window = UIApplication.shared.delegate?.window {
            return window?.avDisplayManager
        }
        return nil
    }

    private var frameRate: Float? {
        if let array = self.player.media.tracksInformation as? [[String:Any]] {
            for dict in array {
                if let frame_rate_num = dict["frame_rate_num"] as? NSNumber,
                   let frame_rate_den = dict["frame_rate_den"] as? NSNumber {
                    if frame_rate_den == 0 {
                        return 50
                    }
                    let frameRate = frame_rate_num.floatValue / frame_rate_den.floatValue
                    return round(frameRate)
                }
            }
        }
        return nil
    }

    func updateCriteriaToDeatult() {
        if let manager = self.displayManager {
            manager.preferredDisplayCriteria = nil
        }
    }

    func updateCriteria() {
        if self.needUpdateCriteria,
           let manager = self.displayManager,
           let frameRate = self.frameRate,
           let criteria = AVDisplayCriteria(refreshRate: frameRate, videoDynamicRange: DynamicRange.SDR.rawValue) {
            self.setDisplayCriteria(manager, displayCriteria: criteria)
            Log.write("update Movie rate: \(frameRate)")
            self.needUpdateCriteria = false
        }
    }

    private func setDisplayCriteria(_ manager:AVDisplayManager, displayCriteria:AVDisplayCriteria?) {
        if manager.isDisplayCriteriaMatchingEnabled,
           manager.preferredDisplayCriteria?.refreshRate != displayCriteria?.refreshRate {
            self.addModeSwitchObserver()
            manager.preferredDisplayCriteria = displayCriteria
        }
    }

    private func addModeSwitchObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(displayModeSwitchStart), name: NSNotification.Name.AVDisplayManagerModeSwitchStart, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(displayModeSwitchEnd), name: NSNotification.Name.AVDisplayManagerModeSwitchEnd, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(displayModeSwitchSettingsChanged), name: NSNotification.Name.AVDisplayManagerModeSwitchSettingsChanged, object: nil)

    }

    private func removeModeSwitchObserver() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVDisplayManagerModeSwitchEnd, object: nil)
    }

    @objc private func displayModeSwitchStart() {
//        self.player.pause()
    }
    @objc private func displayModeSwitchEnd() {
        self.removeModeSwitchObserver()
//        self.player.play()
    }
    @objc private func displayModeSwitchSettingsChanged() {

    }
}
