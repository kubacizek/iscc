//
//  SCCMovie+ext.swift
//  StreamCinema.atv
//
//  Created by SCC on 06/02/2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit

extension SCCMovie {
    var infoPanel: InfoPanelData {
        return InfoPanelData(title: self.title,
                             desc: self.desc ?? "",
                             posterURL: self.poster,
                             bannerURL: self.banner,
                             clearLogoURL: self.logo)
    }
}

//MARK: - mearge objects
extension SCCMovie {
    
    func isExistInWatchList() -> Bool {
        let data = WatchedWrapper.getWatched(type: self.traktType.filterType, entity: .sccWatchList)
        if data.contains(where: { $0.ids == self.ids }) {
            return true
        }
        return false
    }
    
    @discardableResult
    mutating func getCurrentState() -> WatchedState {
        if self.traktType == .tvshow {
            if WatchedWrapper.existWatchedEpisode(for: self.rootIDs) {
                return .paused
            }
            return .none
        }
        self.uprdateProgressFromSavedScc()
        
        if self.progress == nil {
            return .none
        }
        else if self.progress == 0 {
            return .done
        } else {
            return .paused
        }
    }
    
    mutating func uprdateProgressFromSavedScc() {
        guard let ids = self.ids else { return }
        if self.traktType == .movie {
            let watched = WatchedWrapper.movie(movieID: ids.sccID, traktID: (ids.trakt != nil) ? "\(ids.trakt!)" : nil )
            self.progress = watched?.progress
            self.time = watched?.time
        } else if self.traktType == .tvshow {
            let watched = WatchedWrapper.show(movieID: ids.sccID, traktID: (ids.trakt != nil) ? "\(ids.trakt!)" : nil )
            self.progress = watched?.progress
            self.time = watched?.time
        } else if self.traktType == .episode {
            let watched = WatchedWrapper.episode(movieID: ids.sccID, traktID: (ids.trakt != nil) ? "\(ids.trakt!)" : nil )
            self.progress = watched?.progress
            self.time = watched?.time
        } else  if self.traktType == .season {
            let watched = WatchedWrapper.season(movieID: ids.sccID, traktID: (ids.trakt != nil) ? "\(ids.trakt!)" : nil )
            self.progress = watched?.progress
            self.time = watched?.time
        }
    }
    
    public mutating func mearge(with object:SCCMovie) {
        if self.title != object.title  {
            self.title = object.title
        }
        if self.desc != object.desc, object.desc != nil {
            self.desc = object.desc
        }
        
        self.auiodLangs = object.auiodLangs
        self.traktType = object.traktType
        self.rating = object.rating
        
        if object.rootIDs != nil {
            self.rootIDs = object.rootIDs
        }
        if object.seasonIDs != nil {
            self.seasonIDs = object.seasonIDs
        }
        if object.episodeIDs != nil {
            self.episodeIDs = object.episodeIDs
        }

        if object.season != nil {
            self.season = object.season
        }
        if object.season != nil {
            self.season = object.season
        }
        
        self.poster = object.poster
        self.banner = object.banner
        self.fanart = object.fanart
        self.logo = object.logo
        self.thumb = object.thumb
        
        if object.date != nil {
            self.date = object.date
        }
        
        self.trailer = object.trailer
        self.movieData = object.movieData
        self.state = object.state
        if self.cast == nil {
            self.cast = object.cast
        }
    }
}
