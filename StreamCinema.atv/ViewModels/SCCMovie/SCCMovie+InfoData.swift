//
//  SCCMovie+InfoData.swift
//  StreamCinema.atv
//
//  Created by SCC on 30/01/2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit

//MARK: - SCC data parse
extension InfoData {
    func getSCCMovie() -> SCCMovie {
        guard let id = self.id,
              let infoLabel = self.source?.getInfoLabels(),
              let type = self.source?.infoLabels?.getMediaType()
        else { return SCCMovie() }
        
        let desc = infoLabel.plot
        var title = infoLabel.title
        let currentIDs = self.source?.services?.getIDs(with: id)
        var rootIDs:IDs?
        var seasonIDs:IDs?
        var episodeIDs:IDs?
        
        switch type {
        case .movie,
             .tvshow:
            rootIDs = currentIDs
        case .season:
            if title == nil {
                title = String(localized: .season)
                if let seasionNumber = self.source?.infoLabels?.season {
                    title = "\(seasionNumber) - \(String(localized: .season))"
                }
            }
            seasonIDs = currentIDs
        case .episode:
            if title == nil {
                title = String(localized: .episode)
                if let episodeNumber = self.source?.infoLabels?.episode {
                    title = "\(episodeNumber) - \(String(localized: .episode))"
                }
            }
            episodeIDs = currentIDs
        }
        
        var posterUrl:URL?
        if let posterString = infoLabel.art?.poster,
           !posterString.contains("null") {
            posterUrl = URL(string: posterString)
        }
        
        var bannerUrl:URL?
        if let bannerString = infoLabel.art?.banner,
           !bannerString.contains("null") {
            bannerUrl = URL(string: bannerString)
        }
        
        var fanArtURL:URL?
        if let fanartString = infoLabel.art?.fanart,
           !fanartString.contains("null") {
            fanArtURL = URL(string: fanartString)
        }
        
        var clearlogoURL:URL?
        if let clearlogoString = infoLabel.art?.clearlogo,
           !clearlogoString.contains("null") {
            clearlogoURL = URL(string: clearlogoString)
        }
        
        let audio = self.source?.availableStreams?.languages?.audio?.map
        
        let movieData = self.source?.getMovieData() ?? [[:]]
        let ratingData = self.source?.getRating() ?? Rating()
        let trailer = self.source?.getTrailer()
        let episode = self.source?.infoLabels?.episode
        let season = self.source?.infoLabels?.season
        
        let cast = self.source?.cast
        
        var movie = SCCMovie(title: (title ?? ""), traktType: type.sccT, rating: ratingData, auiodLangs: audio,
                        rootIDs: rootIDs, seasonIDs: seasonIDs, episodeIDs: episodeIDs,
                        season: season, episode: episode, poster: posterUrl, banner: bannerUrl, fanart: fanArtURL, logo: clearlogoURL, thumb: nil, date: nil, trailer: trailer, desc: desc, movieData: movieData, state: WatchedState.none, cast:cast, progress: nil, time: nil, year: self.source?.infoLabels?.year)
        #if TARGET_OS_TVOS
        movie.uprdateProgressFromSavedScc()
        #endif
        return movie
    }
}


