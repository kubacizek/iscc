//
//  SCCMovie+Images.swift
//  StreamCinema.atv
//
//  Created by SCC on 30/01/2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit
import TMDBKit
import FanArtKit

//MARK: - Fanart and TMDB images
extension SCCMovie {
    public func cellBacgroundImage(completition: @escaping (URL?)->Void) {
        if let thumb = self.thumb {
            completition(thumb)
        }
        else if let fanArt = self.fanart {
            completition(fanArt)
        } else {
            self.fetchBacgroundImage(completition: completition)
        }
    }
    
    private func fetchBacgroundImage(completition: @escaping (URL?)->Void) {
        self.tmdbBackdrop { url in
            if let url = url {
                completition(url)
            } else {
                self.fanArtThumb(completition: completition)
            }
        }
    }
    
    private func tmdbBackdrop(completition: @escaping (URL?)->Void ) {
        if let tmdbID = self.ids?.tmdb {
            if self.traktType == .movie {
                MovieMDB.images(movieID: tmdbID, language: nil) { (value, images) in
                    self.tmdbImage(value, images, completition: completition)
                }
            } else if self.traktType == .episode,
                      let tmdb = self.ids?.tmdb,
                      let episode = self.episode,
                      let season = self.season {
                TVEpisodesMDB.images(tvShowId: tmdb, seasonNumber: season, episodeNumber: episode) { (value, images) in
                    self.tmdbImage(value, images, completition: completition)
                }
            } else {
                TVMDB.images(tvShowID: tmdbID, language: nil) { (value, images) in
                    self.tmdbImage(value, images, completition: completition)
                }
            }
        }
    }
    
    private func tmdbImage(_ value: ClientReturn, _ images: ImagesMDB?, completition: @escaping (URL?)->Void ) {
        if let backdrop = value.json?["backdrop_path"],
           backdrop != nil,
           let backdropURL = URL(string: "https://image.tmdb.org/t/p/w500\(backdrop)"){
            completition(backdropURL)
        }
        else if let thumb = images?.backdrops.first?.file_path,
           let backdrop = URL(string: "https://image.tmdb.org/t/p/w500\(thumb)") {
            completition(backdrop)
        }
        else if let stillPath = images?.stills.first?.file_path,
                let backdrop = URL(string: "https://image.tmdb.org/t/p/w500\(stillPath)") {
            completition(backdrop)
        }
        else {
            completition(nil)
        }
    }
    
    private func fanArtThumb(completition: @escaping (URL?)->Void) {
        if let tmdbID = self.ids?.tmdb {
            if self.traktType == .movie {
                FanartClient.shared.movie(tmdb: tmdbID) { fanArtResult -> (Void) in
                    switch fanArtResult {
                    case .success(result: let result):
                        if let strURL = result.moviethumb?.first?.url {
                            completition(URL(string: strURL))
                        } else {
                            completition(nil)
                        }
                    case .error(reason: let reason):
                        Log.write("fanArtThumb movie reason \(reason)")
                        completition(nil)
                    }
                }
            } else {
                FanartClient.shared.show(identifier: tmdbID) { fanArtResult -> (Void) in
                    switch fanArtResult {
                    case .success(result: let result):
                        if let strURL = result.tvthumb?.first?.url {
                            completition(URL(string: strURL))
                        } else {
                            completition(nil)
                        }
                    case .error(reason: let reason):
                        Log.write("fanArtThumb show reason \(reason)")
                        completition(nil)
                    }
                }
            }
        } else {
            completition(nil)
        }
    }
}
