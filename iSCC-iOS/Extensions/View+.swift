//
//  View+.swift
//  iSCC-iOS
//
//  Created by Martin Matějka on 19.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import SwiftUI

extension View {

    @ViewBuilder
    func `if`<Content: View>(_ condition: Bool, content: (Self) -> Content)
        -> some View
    {
        if condition {
            content(self)
        } else {
            self
        }
    }

    @ViewBuilder
    func `if`<T, Content: View>(
        _ maybeObject: T?,
        content: (T, Self) -> Content
    )
        -> some View
    {
        if let object = maybeObject {
            content(object, self)
        } else {
            self
        }
    }

    func toAny() -> AnyView {
        AnyView(self)
    }
}
