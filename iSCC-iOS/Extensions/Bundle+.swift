//
//  Bundle+.swift
//  iSCC-iOS
//
//  Created by Martin Matějka on 13.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import SwiftUI


extension Bundle {
    
    static var core: Bundle? {
        return Bundle(identifier: "zone.sc2.StreamCinema.iSCC-iOS")
    }
    
}

extension Image {
    
    init(name: String) {
        self.init(name, bundle: Bundle.core)
    }
    
}
