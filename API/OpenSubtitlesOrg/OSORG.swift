//
//  OSRequest.swift
//  StreamCinema.atv
//
//  Created by SCC on 05/08/2020.
//  Copyright © 2020 SCC. All rights reserved.
//
//https://forum.opensubtitles.org/viewtopic.php?f=8&t=16453&start=60
// Download http://dl.opensubtitles.org/en/download/src-api/vrf-19d50c5e/sid-{YOUR_SID_HERE}/filead/1951976249.gz
// search https://rest.opensubtitles.org/search/episode-5/imdbid-0455275/season-5

import UIKit
import Alamofire
import Moya

extension Provider {
    static let osOrgRequest: MoyaProvider<OSORGRequest> = NetworkingClient.provider()
}

enum OSORGRequest: TargetType {
    
    case login(name:String, pass:String)
    case findWith(imdbid:String, lang:[String])
    case download(id:String, name:String)
    
    var baseURL: URL {
        switch self {
        case .findWith, .login:
            return URLSettings.OpenSubtitlesORG
        case .download:
            return URLSettings.OpenSubtitlesOrgDl
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .login:
            return .post
        case .findWith:
            return .post
        case .download:
            return .post
        }
    }
    
    var headers: [String: String]? {
        var headers = ["Accept":"application/json","Content-Type":"application/json"]
        switch self {
        case .login:
            break
        case .findWith(imdbid: let imdbid, lang: _):
            headers["Authorization"] = imdbid
        case .download(id: _ , name: let name):
            headers["Authorization"] = name
        }
        return headers
    }
    
    var path: String {
        switch self {
        case .login:
            return self.loginPath
        case .findWith(imdbid: _, lang: _):
            return self.findPaht
        case .download:
            return self.downloadPath
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var parameterEncoding: Moya.ParameterEncoding {
        switch self {
        default:
            return URLEncoding.default
        }
    }
    
    var task: Task {
        switch self {
        case .login(name:let name, pass:let pass):
            return .requestParameters(parameters: ["username": name, "password": pass], encoding: JSONEncoding.default)
        case .findWith(imdbid: let imdbid, lang: let lang):
            return .requestParameters(parameters: ["imdbid": imdbid, "languages": lang], encoding: URLEncoding.queryString)
        case .download(id: let id, name: let name):
            let data = ["file_id":id, "file_name":name, "sub_format":"srt"]
            return .requestParameters(parameters: data, encoding: JSONEncoding.default)
        }
    }
}

extension OSORGRequest {
    var loginPath: String {
        return "/login/"
    }
    var findPaht: String {
        return "/find/"
    }
    var downloadPath: String {
        return "/download/"
    }
}

