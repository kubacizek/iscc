//
//  WSService.swift
//  StreamCinema
//
//  Created by SCC on 26/05/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation
import Moya
import XMLMapper
import Combine

final class WSService {

    enum WSServiceError: Error {
        case decodingData
        case missingLoginLink
        case encodingResponseFailed
        case didNotReceivedLoginToken
        case didNotreceivedSaltInformation
        case parsingOfUserInformationsFailed
        case unknownLogoutProblem
    }


    let apiClient = Provider.ws


    public func getStreamLink(params: [String:Any]) -> AnyPublisher<WSStreamModel, Error> {
        return apiClient
            .requestData(request: WSRequest.getFile(params: params))
            .tryMap { (data) -> WSStreamModel in
                guard let respString = String(data: data, encoding: .utf8) else { throw WSServiceError.decodingData }
                guard let login = XMLMapper<WSStreamModel>().map(XMLString: respString), login.link != nil else { throw WSServiceError.missingLoginLink }
                return login
            }.eraseToAnyPublisher()
    }

    
    public func logout(device_uuid: UUID, token wst:String) -> AnyPublisher<Bool, Error> {
        var data:[String:Any] = [:]
        data["device_uuid"] = device_uuid
        data["wst"] = wst

        return apiClient
            .requestData(request: WSRequest.logout(with: data))
            .tryMap { data -> Bool in
                guard let respString = String(data: data, encoding: .utf8) else { throw WSServiceError.encodingResponseFailed }
                let resultModel = XMLMapper<ResultEmptyDataModel>().map(XMLString: respString)
                if let result = resultModel, result.status == "OK" {
                       return true
                } else {
                    throw WSServiceError.unknownLogoutProblem
                }
            }
            .eraseToAnyPublisher()
    }
    
    public func login(params: [String:Any]) -> AnyPublisher<LoginModel, Error> {

        apiClient
            .requestData(request: WSRequest.login(params: params))
            .tryMap { data -> LoginModel in
                guard let respString = String(data: data, encoding: .utf8) else { throw WSServiceError.encodingResponseFailed }
                let login = XMLMapper<LoginModel>().map(XMLString: respString)
                if let login = login, login.token != nil {
                    return login
                } else {
                    throw WSServiceError.didNotReceivedLoginToken
                }
            }
            .eraseToAnyPublisher()
    }

    
    public func getSalt(salt: String) -> AnyPublisher<SaltModel, Error> {

        apiClient
            .requestData(request: WSRequest.getSalt(name: salt))
            .tryMap { (data) -> SaltModel in
                guard let respString = String(data: data, encoding: .utf8) else { throw WSServiceError.encodingResponseFailed }
                let salt = XMLMapper<SaltModel>().map(XMLString: respString)
                if let salt = salt, salt.status == "OK", salt.salt != nil {
                    return salt
                } else {
                    throw WSServiceError.didNotreceivedSaltInformation
                }
            }
            .eraseToAnyPublisher()
    }
    
    public func getUserData(params: [String:Any]) -> AnyPublisher<UserModel, Error>  {
        apiClient
            .requestData(request: WSRequest.userData(with: params))
            .tryMap { (data) -> UserModel in
                guard let respString = String(data: data, encoding: .utf8) else { throw WSServiceError.encodingResponseFailed }
                let userData = XMLMapper<UserModel>().map(XMLString: respString)
                if let user = userData {
                    return user
                } else {
                    throw WSServiceError.parsingOfUserInformationsFailed
                }
            }
            .eraseToAnyPublisher()
    }
}
