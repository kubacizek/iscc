//
//  GitLabService.swift
//  StreamCinema.atv
//
//  Created by SCC on 12/11/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation
import Moya
import Combine


final class ServiceGitLab {

    enum GitLabError: Error {
        case noReleaseFounded

        var localizedDescription: String {
            switch self {
            case .noReleaseFounded: return "Any stable public release was not found. Please contact the administrator about it."
            }
        }
    }

    let apiClient = Provider.gitRequest

    //MARK: - GitLabReleases

    public func getReleases() -> AnyPublisher<GitLabRelease, Error> {
        apiClient
            .requestWithoutMapping(request: GitRequest.getVersion)
            .tryMap { response -> [GitLabRelease] in
                let decoder = JSONDecoder()
                return try decoder.decode([GitLabRelease].self, from:  response.data)
            }
            .tryMap {
                if let release = $0.first {
                    return release
                } else {
                    throw GitLabError.noReleaseFounded
                }
            }
            .eraseToAnyPublisher()

    }
}
