//
//  OSProgressModel.swift
//  StreamCinema.atv
//
//  Created by SCC on 05/08/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation

struct OSProgressModel {
    let progress: Int?
    let path: String?
}
