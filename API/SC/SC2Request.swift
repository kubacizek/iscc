//
//  SC2_ApiRequest.swift
//  StreamCinema
//
//  Created by SCC on 08/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import Alamofire
import Moya

extension Provider {
    static let sc2Request: MoyaProvider<SC2Request> = NetworkingClient.provider()
}
///https://plugin.sc2.zone/api/media/filter/startsWithSimple/count/titles?type=movie&value=
///http://plugin.sc2.zone/api/stream/probe/L932p6cp3S
///https://plugin.sc2.zone/api/media/tv/2020-08-14T00:00:00.000Z/program?access_token=5ze3qiq6lmoartnpktvl
enum SC2Request: TargetType {
    
    case getMedia(mediaID:String)
    case getMediaFrom(Service:FilterService, mediaID:String)
    case filter(with:FilterModel)
    case getStream(mediaID:String)
    case azSearch(filter:FilterModel)
    case setAsPlayed(mediaID:String)
    case tvProgram(station:String, date:String, page: Int)
    
    var baseURL: URL {
        return URLSettings.sc2Url
    }
    
    var method: Moya.Method {
        switch self {
        case .setAsPlayed:
            return .post
        case .filter,
             .azSearch,
             .getMedia,
             .getMediaFrom,
             .tvProgram,
             .getStream:
            return .get
        }
    }
    
    var headers: [String: String]? {
        if let uuid = UIDevice.current.identifierForVendor?.uuidString,
            let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            let header = ["X-Uuid":uuid, "User-Agent":"com.streamcinemacommunity.atv" + "_" + appVersion]
            return header
        }
        return nil
    }
    
    var path: String {
        switch self {
        case .azSearch(let model):
            guard let count = model.count?.rawValue else { return "" }
            return self.filterPath + model.name.rawValue + self.azSearch + count
        case .filter(let filterModel):
            return self.filterPath + filterModel.name.rawValue
        case .getMedia(let mediaID):
            return self.mediaPath + mediaID
        case .getMediaFrom(let service, let mediaID):
            return self.servicePath + service.rawValue + "/" + mediaID
        case .getStream(let mediaID):
            return self.mediaPath + mediaID + self.streamsPath
        case .setAsPlayed(mediaID: let mediaID):
            return self.playedPath + mediaID
        case .tvProgram(station: _, date: let date, page:_):
            return self.tvProgram + date + "T00:00:00.000Z" + "/program"
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
//    var parameterEncoding: Moya.ParameterEncoding {
//        switch self {
//        case .filter:
//            return ParameterEncoding
//        default:
//            return URLEncoding.default
//        }
//    }
    
    var task: Task {
        switch self {
        case .filter(let model),
             .azSearch(let model):
            let test = URLEncoding(destination: .methodDependent, arrayEncoding: .noBrackets, boolEncoding: .numeric)
            return .requestParameters(parameters: model.getDictionnary(), encoding: test)
        case .getMedia,
             .getMediaFrom,
             .getStream:
            guard let token = Bundle.main.infoDictionary?["CFBundleSCC"]  else { return .requestPlain }
            let test = URLEncoding(destination: .methodDependent, arrayEncoding: .noBrackets, boolEncoding: .numeric)
            return .requestParameters(parameters: ["access_token":token], encoding: test)
        case .setAsPlayed:
            guard let token = Bundle.main.infoDictionary?["CFBundleSCC"]  else { return .requestPlain }
            return .requestParameters(parameters: ["access_token":token], encoding: URLEncoding.queryString)
        case .tvProgram(station: let station, date: _, page: let page):
            guard let token = Bundle.main.infoDictionary?["CFBundleSCC"]  else { return .requestPlain }
            return .requestParameters(parameters: ["access_token":token, "station":station, "page":page], encoding: URLEncoding.queryString)
        }
    }
}

extension SC2Request {
    var playedPath: String {
        return "/media/played/"
    }
    var mediaPath: String {
        return "/media/"
    }
    var servicePath: String {
        return "/media/service/"
    }
    var filterPath: String {
        return "/media/filter/"
    }
    var streamsPath: String {
        return "/streams"
    }
    var azSearch: String {
        return "/count/"
    }
    var tvProgram: String {
        return "media/tv/"
    }
}
