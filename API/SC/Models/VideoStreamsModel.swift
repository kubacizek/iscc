//
//  StreamsModel.swift
//  StreamCinema
//
//  Created by SCC on 08/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import Combine

// MARK: - Stream
struct VideoStreams: DataConvertible, Hashable, Model {
    var data: [VideoStream]
    
    func getSortedData() -> [VideoStream] {
        if UIDevice().type == .AppleTV {
            let filtredData = self.data.filter({ $0.video?.first?.isVideoPlayableAtATV4G() ?? false })
            return filtredData.sorted { (lhs, rhs) -> Bool in
                return self.sort(lhs: lhs, rhs: rhs)
            }
        }
        return self.data.sorted { (lhs, rhs) -> Bool in
            return self.sort(lhs: lhs, rhs: rhs)
        }
    }
    
    private func sort(lhs:VideoStream, rhs:VideoStream ) -> Bool{
        if let lVideo = lhs.video?.first, let rVideo = rhs.video?.first {
            if lVideo.getResolution() == rVideo.getResolution() {
                if lVideo.hdr == true, rVideo.hdr == true {
                    if lVideo.codec == "HEVC" {
                        return true
                    } else if rVideo.codec == "HEVC" {
                        return false
                    }
                    return (lhs.size ?? 0) > (rhs.size ?? 0 )
                } else if lVideo.hdr == true {
                    return true
                } else if rVideo.hdr == true {
                    return false
                } else if lVideo.codec == "HEVC" {
                    return true
                } else if rVideo.codec == "HEVC" {
                    return false
                } else {
                    return (lhs.size ?? 0) > (rhs.size ?? 0 )
                }
            }
            return lVideo.getResolution() < rVideo.getResolution()
        }
        return false
    }
    
    func isAutoSelectVideo(_ lastAudio:String? = nil) -> VideoStream? {
        guard 
            let appdel = UIApplication.shared.delegate as? AppDelegate,
            self.data.count > 0 else { return nil }

        let audio = AudioLanguage(audioString: lastAudio)
        let speed = appdel.appData.speed.lastSpeed
        
        let sortedStreams = self.getSortedData()
        
        let langStreams = self.getStreams(by: audio.getAudioString(), streams: sortedStreams)
        let langAndSpeed = self.getStream(with: speed, streams: langStreams)
        
        if langAndSpeed.count > 0 {
            return langAndSpeed.first
        } else if langStreams.count > 0 {
            return langStreams.last
        } else {
            return self.getStream(with: speed, streams: sortedStreams).first
        }
    }

    private func getStreams(by lang:String, streams:[VideoStream]) -> [VideoStream] {
        return streams.filter({ $0.audio?.filter({ $0.language?.uppercased() == lang.uppercased() }).count ?? 0 > 0 })
    }
    
//    func getVideoStream(audioLang:AudioLanguage) -> VideoStream? { //OLD odstranit?
//        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return nil }
//        let bitrate = appDelegate.appData.speed.lastSpeed
//        let data = self.getSortedData()
//        let audioString = audioLang.getAudioString()
//        let bitrateStreams =  self.getStream(with: bitrate, streams: data)
//
//        let streamOptions = bitrateStreams.filter({ $0.audio?.filter({ $0.language?.uppercased() == audioString }).count ?? 0 > 0 })
//        if streamOptions.count > 0 {
//            return streamOptions.first
//        } else {
//            return bitrateStreams.first
//        }
//    }
    
    private func getStream(with bitrate:Double, streams:[VideoStream]) -> [VideoStream] {
        if bitrate == 0 {
            return streams
        }
        if streams.count == 0 {
            return []
        }
        let optimalBitrate = streams.filter({
            if let streamBitrate = $0.getBitrate() {
                if streamBitrate <= bitrate * 0.9 {
                    return true
                }
            }
            return false
        })
        return optimalBitrate
    }
    
    static func empty<T>() -> T where T: Model {
        let rides = VideoStreams(data: [])
        guard let tRides = rides as? T else {
            fatalError()
        }
        return tRides
    }

    init(data: [VideoStream]) {
        self.data = data.sorted(by: { ($0.size ?? 0) > ($1.size ?? 0 ) })
    }
    
}

final public class AudioLanguage {
    private let audioString: String?
    private let en:[String] = ["English", "EN", "ENG", "ANG"]
    private let cz:[String] = ["Czech", "CZ", "CS", "CZE", "CES"]
    private let sk:[String] = ["Slovak","Slovenština", "SK", "SLO", "SLK"]
    
    init(audioString: String?) {
        self.audioString = audioString?.lowercased()
    }
    
    func getAudioString() -> String {
        if let lang = self.getLangString() {
            return lang
        }
        if
            let system = Locale.preferredLanguages.first,
            let lang = system.uppercased().split(separator: "-").first {
            return String(lang)
        }
        return "EN"
    }
    
    private func getLangString() -> String? {
        if let audio = self.audioString {
            if self.contains(string: audio, in: self.en) {
               return "EN"
            } else if self.contains(string: audio, in: self.cz) {
                return "CS"
            } else if self.contains(string: audio, in: self.sk) {
                return "SK"
            }
        }
        return nil
    }
    
    private func contains(string:String, in array: [String]) -> Bool {
        for testedString in array {
            if string.lowercased().contains(testedString.lowercased()) == true {
                return true
            }
        }
        return false
    }
    
    static func language(_ title:String) -> String? {
        let audio = AudioLanguage(audioString: title)
        return audio.getLangString()
    }
}

struct VideoStream: DataConvertible, Hashable, Model {

    let id, provider, ident: String?
    let duration: Double?
    let size: Int?
    let validated: Bool?
    let audio: [AudioInfo]?
    let video: [VideoInfo]?
    let subtitles: [SubtitleInfo]?
    

    func getStreamInfo(isSselected: Bool) -> String {
        var info:String = ""
        if isSselected {
            info = "✓  "
        }
        if let byteCount = self.size {
            info += Units(bytes: Int64(byteCount)).getReadableUnit() + ","
        }
        if let video = self.getVideoInfo() {
            info += " " + video
        }
        if let audio = self.getAudioTracks() {
            info += " " + audio
        }
        if let subtitles = self.getSubtitlesTracks() {
            info += " " + subtitles
        }
        if let bitrate = self.getBitrateString() {
            info += " " + bitrate
        }
        if let codec = self.video?.first?.codec {
            info += " " + codec.uppercased()
        }
        
        return info
    }
    
    private func getVideoInfo() -> String? {
        if let video = self.video?.first {
            var videoString:String = video.getResolutionString()
            
            if video.hdr == true {
                videoString += " HDR"
            }
            
            videoString += ","
            
            return videoString
        }
        return nil
    }
    
    private func getSubtitlesTracks() -> String? {
        if let subtitles = self.subtitles {
            let subtitlesTracksUniq = self.removeDuplicateElements(subtitles)
            let subtitlesString = subtitlesTracksUniq.map({$0.getLang()}).joined(separator: "/")
            if !subtitlesString.isEmpty {
                return "tit: \(subtitlesString),"
            }
        }
        return nil
    }
    
    private func getAudioTracks() -> String? {
        if let audioTracks = self.audio {
            let audioTracksUniq = self.removeDuplicateElements(audioTracks)
            let audioString = audioTracksUniq.map({$0.getLang()}).joined(separator: "/")
            if !audioString.isEmpty {
                return "jaz: \(audioString),"
            }
        }
        return nil
    }
    
    func removeDuplicateElements(_ audios: [AudioInfo]) -> [AudioInfo] {
        var uniquePosts:[AudioInfo] = []
        for post in audios {
            if !uniquePosts.contains(where: {$0.language == post.language }) {
                uniquePosts.append(post)
            }
        }
        return uniquePosts
    }
    
    func removeDuplicateElements(_ subtitles: [SubtitleInfo]) -> [SubtitleInfo] {
        var uniquePosts:[SubtitleInfo] = []
        for post in subtitles {
            if !uniquePosts.contains(where: {$0.language == post.language }) {
                uniquePosts.append(post)
            }
        }
        return uniquePosts
    }
    
    private func getBitrateString() -> String? {
        if let bitrate = self.getBitrate() {
            return String(format: (bitrate > 5) ? "%.0f Mbps" : "%.1f Mbps", bitrate)
        }
        return nil
    }
    
    func getBitrate() -> Double? {
        if let duration = self.video?.first?.duration,
            let size = self.size {
            let mb_size = Units(bytes: Int64(size)).megabytes
            let bitrate = (mb_size / (Double(duration / 60) * 0.0075))/1000
            return bitrate
        }
        return nil
    }
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case provider, ident, duration, size, validated, audio, video, subtitles
    }
    
    static func empty<T>() -> T where T: Model {
        let audioInfo = VideoStream(id: "", provider: "", ident: "", duration: 0.0, size: 0, validated: false, audio: [], video: [], subtitles: [])
        guard let emptyAudioInfo = audioInfo as?T else {
            fatalError()
        }
        return emptyAudioInfo
    }
}

// MARK: - Audio
struct AudioInfo: DataConvertible, Hashable, Model {
    let id, language, codec: String?
    let channels: Int?

    func getLang() -> String {
        if let lang = self.language {
            return lang
        }
        return "NA"
    }
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case language, codec, channels
    }
    
    static func empty<T>() -> T where T: Model {
        let audioInfo = AudioInfo(id: "", language: "", codec: "", channels: 0)
        guard let emptyAudioInfo = audioInfo as?T else {
            fatalError()
        }
        return emptyAudioInfo
    }
}

// MARK: - Subtitle
struct SubtitleInfo: DataConvertible, Hashable, Model {
    let id, language: String?
    let forced: Bool?

    func getLang() -> String {
        if let lang = self.language {
            return lang
        }
        return "NA"
    }
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case language, forced
    }
    
    static func empty<T>() -> T where T: Model {
        let subtitleInfo = SubtitleInfo(id: "", language: "", forced: false)
        guard let emptySubtitleInfo = subtitleInfo as?T else {
            fatalError()
        }
        return emptySubtitleInfo
    }
}

// MARK: - Video
enum Resolution: String, Comparable {
    case qMoreThan16k = ">16k"
    case q16k = "16k"
    case qMoreThan8k = ">8k"
    case q8k = "8k"
    case qMoreThan4k = ">4k"
    case q4k = "4k"
    case qMoreThanQuadHD = ">QuadHD"
    case qQuadHD = "QuadHD"
    case qMoreThanFullHD  = ">FullHD"
    case qFullHD = "FullHD"
    case qHDReady = "HDReady"
    case qSD = "SD"
    case q360p = "360p"
    case q240p = "240p"
    case unknown

    private var sortOrder: Int {
            switch self {
            case .qMoreThan16k:
                return 0
            case .q16k:
                return 1
            case .qMoreThan8k:
                return 2
            case .q8k:
                return 3
            case .qMoreThan4k:
                return 4
            case .q4k:
                return 5
            case .qMoreThanQuadHD:
                return 6
            case .qQuadHD:
                return 7
            case .qMoreThanFullHD:
                return 8
            case .qFullHD:
                return 9
            case .qHDReady:
                return 10
            case .qSD:
                return 11
            case .q360p:
                return 12
            case .q240p:
                return 13
            case .unknown:
                return 14
            }
    }
    
    public var image: UIImage? {
        switch self {
        case .qMoreThan16k:
            return UIImage(named: "16k")
        case .q16k:
            return UIImage(named: "16k")
        case .qMoreThan8k:
            return UIImage(named: "8k")
        case .q8k:
            return UIImage(named: "8k")
        case .qMoreThan4k:
            return UIImage(named: "4k")
        case .q4k:
            return UIImage(named: "4k")
        case .qMoreThanQuadHD:
            return UIImage(named: "2k")
        case .qQuadHD:
            return UIImage(named: "2k")
        case .qMoreThanFullHD:
            return UIImage(named: "fhd")
        case .qFullHD:
            return UIImage(named: "fhd")
        case .qHDReady:
            return UIImage(named: "hd")
        case .qSD:
            return UIImage(named: "sd")
        case .q360p:
            return UIImage(named: "360")
        case .q240p:
            return UIImage(named: "240")
        case .unknown:
            return UIImage(named: "240")
        }
    }
    
    static func ==(lhs: Resolution, rhs: Resolution) -> Bool {
            return lhs.sortOrder == rhs.sortOrder
        }

    static func <(lhs: Resolution, rhs: Resolution) -> Bool {
       return lhs.sortOrder < rhs.sortOrder
    }
}

extension CGSize {
    func getResolution() -> Resolution {
        if width        >  15360 {
            return .qMoreThan16k
        } else if width == 15360 { // typ. 15360x8640
            return .q16k
        } else if width >   7680 {
            return .qMoreThan8k
        } else if self.width ==  7680 { // typ. 7680x4320
            return .q8k
        } else if self.width >   3840 {
            return .qMoreThan4k
        } else if self.width ==  3840 { // typ. 3840x2160 (16:9), 3840x1604 (2.39:1) etc.
            return .q4k
        } else if self.width >   2560 {
            return .qMoreThanQuadHD
        } else if self.width ==  2560 { // typ. 2560x1440
            return .qQuadHD
        } else if self.width >   1920 {
            return .qMoreThanFullHD
        } else if self.width ==  1920 { // typ. 1920x1080
            return .qFullHD
        } else if self.width >=  1280 { // typ. 1280x720
            return .qHDReady
        } else if self.width >=   640 { // typ. 704x576 4:3, less common 1024x576 16:9, 640x480 4:3 VGA, 848x480 16:9
            return .qSD
        } else if self.width >=   480 { // typ. 480x360
            return .q360p
        } else if self.width >=   352 { // typ. 352x240
            return .q240p
        }
        return .unknown
    }
}

struct VideoInfo: DataConvertible, Hashable, Model {
    let id: String?
    let width, height: Int?
    let aspect, duration: Double?
    let codec: String?
    let hdr, the3D: Bool?
    
    func isVideoPlayableAtATV4G() -> Bool {
        if let width = self.width, let height = self.height {
            let videoSize = CGSize(width: width, height: height)
            if videoSize.width > 15360 || videoSize.width == 15360 || videoSize.width > 7680 || videoSize.width == 7680 || videoSize.width > 3840 || videoSize.width == 3840 || videoSize.width > 2560 || videoSize.width == 2560 {
                return false
            }
        }
        if hdr == true {
            return false
        }
        return true
    }
    
    func getResolutionString() -> String {
        if let width = self.width, let height = self.height {
            let videoSize = CGSize(width: width, height: height)
            if videoSize.width        >  15360 {
                return ">16k"
            } else if videoSize.width == 15360 { // typ. 15360x8640
                return "16k"
            } else if videoSize.width >   7680 {
                return ">8k"
            } else if videoSize.width ==  7680 { // typ. 7680x4320
                return "8k"
            } else if videoSize.width >   3840 {
                return ">4k"
            } else if videoSize.width ==  3840 { // typ. 3840x2160 (16:9), 3840x1604 (2.39:1) etc.
                return "4k"
            } else if videoSize.width >   2560 {
                return ">QuadHD"
            } else if videoSize.width ==  2560 { // typ. 2560x1440
                return "QuadHD"
            } else if videoSize.width >   1920 {
                return ">FullHD"
            } else if videoSize.width ==  1920 { // typ. 1920x1080
                return "FullHD"
            } else if videoSize.width >=  1280 { // typ. 1280x720
                return "HD Ready"
            } else if videoSize.width >=   640 { // typ. 704x576 4:3, less common 1024x576 16:9, 640x480 4:3 VGA, 848x480 16:9
                return "SD \(width)x\(height)"
            } else if videoSize.width >=   480 { // typ. 480x360
                return "360p"
            } else if videoSize.width >=   352 { // typ. 352x240
                return "240p"
            } else {
                return "\(width)x\(height)"
            }
        }
        return ""
    }
    
    func getResolution() -> Resolution {
        if let width = self.width, let height = self.height {
            let videoSize = CGSize(width: width, height: height)
            if videoSize.width        >  15360 {
                return .qMoreThan16k
            } else if videoSize.width == 15360 { // typ. 15360x8640
                return .q16k
            } else if videoSize.width >   7680 {
                return .qMoreThan8k
            } else if videoSize.width ==  7680 { // typ. 7680x4320
                return .q8k
            } else if videoSize.width >   3840 {
                return .qMoreThan4k
            } else if videoSize.width ==  3840 { // typ. 3840x2160 (16:9), 3840x1604 (2.39:1) etc.
                return .q4k
            } else if videoSize.width >   2560 {
                return .qMoreThanQuadHD
            } else if videoSize.width ==  2560 { // typ. 2560x1440
                return .qQuadHD
            } else if videoSize.width >   1920 {
                return .qMoreThanFullHD
            } else if videoSize.width ==  1920 { // typ. 1920x1080
                return .qFullHD
            } else if videoSize.width >=  1280 { // typ. 1280x720
                return .qHDReady
            } else if videoSize.width >=   640 { // typ. 704x576 4:3, less common 1024x576 16:9, 640x480 4:3 VGA, 848x480 16:9
                return .qSD
            } else if videoSize.width >=   480 { // typ. 480x360
                return .q360p
            } else if videoSize.width >=   352 { // typ. 352x240
                return .q240p
            }
        }
        return .unknown
    }

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case width, height, aspect, hdr, duration, codec
        case the3D = "3d"
    }
    
    static func empty<T>() -> T where T: Model {
        let videoInfo = VideoInfo(id: "", width: 0, height: 0, aspect: 0.0, duration: nil,codec: nil, hdr: false, the3D: false)
        guard let epmtyVideoInfo = videoInfo as?T else {
            fatalError()
        }
        return epmtyVideoInfo
    }
}


public struct Units {
  
  public let bytes: Int64
  
  public var kilobytes: Double {
    return Double(bytes) / 1_024
  }
  
  public var megabytes: Double {
    return kilobytes / 1_024
  }
  
  public var gigabytes: Double {
    return megabytes / 1_024
  }
  
  public init(bytes: Int64) {
    self.bytes = bytes
  }
  
  public func getReadableUnit() -> String {
    
    switch bytes {
    case 0..<1_024:
      return "\(bytes) bytes"
    case 1_024..<(1_024 * 1_024):
      return "\(String(format: "%.0f", kilobytes)) KiB"
    case 1_024..<(1_024 * 1_024 * 1_024):
      return "\(String(format: "%.0f", megabytes)) MiB"
    case (1_024 * 1_024 * 1_024)...Int64.max:
      return "\(String(format: "%.1f", gigabytes)) GiB"
    default:
      return "\(bytes) bytes"
    }
  }
}
