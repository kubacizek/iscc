//
//  VLCPlayerViewController.swift
//  IndexOfTV
//
//  Created by Jérémy Marchand on 24/02/2018.
//  Copyright © 2018 Jérémy Marchand. All rights reserved.
//

//VLCKit na stiahnutie posledna verzia https://artifacts.videolan.org/vlc/

import UIKit
import GameController
import TVVLCKit
import Combine
import MediaPlayer
import Kingfisher

final class VLCSettings {
    static var options:[String] {
        get {
            var options = SubtitlesSettings.getSubtitlesOptions()
            options.append("--network-caching=5000")
            return options
        }
    }
}


public class VLCPlayerViewController: UIViewController {

    // MARK: - Static things
    public static func create(viewModel: VLCPlayerViewModel) -> VLCPlayerViewController {
        let storyboard = UIStoryboard(name: "TVVLCPlayer", bundle: Bundle(for: VLCPlayerViewController.self))
        let vc = storyboard.instantiateInitialViewController() as! VLCPlayerViewController
        vc.player = VLCMediaPlayer()
        vc.viewModel = viewModel
        if let link = viewModel.link {
            vc.play(link: link,isContinue: viewModel.isContrinue)
        }
//        vc.player.audio.passthrough = true
//        vc.player.audio.volumeUp()
        return vc
    }

    // MARK: - IBOutlets
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var remainingLabel: UILabel!
    @IBOutlet weak var scrubbingLabel: UILabel!
    @IBOutlet weak var transportBar: ProgressBar!
    @IBOutlet weak var playbackControlView: GradientView!
    @IBOutlet weak var rightActionIndicator: UIImageView!
    @IBOutlet weak var pauseImageView: UIImageView!
    @IBOutlet weak var positionConstraint: NSLayoutConstraint!
    @IBOutlet weak var bufferingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var openingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var actionGesture: LongPressGestureRecogniser!
    @IBOutlet weak var playPauseGesture: UITapGestureRecognizer!
    @IBOutlet weak var cancelGesture: UITapGestureRecognizer!
    @IBOutlet weak var continueView: ContinueView!

    @IBOutlet weak var scrubbingPositionController: ScrubbingPositionController!
    @IBOutlet weak var remoteActionPositionController: RemoteActionPositionController!

    private let playerStateSubject: PassthroughSubject<VLCMediaPlayerState, Never> = PassthroughSubject()
    private let playerTimeSubject: PassthroughSubject<Double, Never> = PassthroughSubject()
    private let errorSubject: PassthroughSubject<Error, Never> = PassthroughSubject()
    private var cancelables: Set<AnyCancellable> = Set()


    // MARK: - Private properties
    private var viewModel: VLCPlayerViewModel!
    private var timer: ResumableTimer?
    var needUpdateCriteria: Bool = true
    var player: VLCMediaPlayer!

    private var positionController: PositionController? {
        didSet {
            guard positionController !== oldValue else {
                return
            }
            oldValue?.isEnabled = false
            positionController?.isEnabled = true
        }
    }

    private var isOpening: Bool = true {
        didSet {
            guard self.viewIfLoaded != nil else {
                return
            }

            guard isOpening != oldValue else {
                return
            }

            if isOpening {
                openingIndicator.startAnimating()
            } else {
                openingIndicator.stopAnimating()
            }
            self.setupPositionController()
        }
    }

    private var isBuffering: Bool = false {
        didSet {
            guard isBuffering != oldValue else { return }
            if isBuffering {
                bufferingIndicator.startAnimating()
            } else {
                bufferingIndicator.stopAnimating()

            }
            rightActionIndicator.isHidden = isBuffering
        }
    }

    private var isTimeToShowNexEpisode:Bool {
        get {
            if self.playerCurrentTime > 0,
               (Double(self.viewModel.countDownRepet) + self.playerCurrentTime) >= self.playerTotalTime,
               (self.player.state == .playing || self.player.state == .buffering) {
                Log.write("isTimeToShowNexEpisode")
                return true
            }
            return false
        }
    }

    private var lastSelectedPanelTabIndex: Int = 0
    private var displayedPanelViewController: UIViewController?
    private var isPanelDisplayed: Bool {
        return displayedPanelViewController != nil
    }

    // MARK: - Life-cycle

    deinit { Log.write("VLCPlayerViewController deinit") }


    public override func viewDidLoad() {
        super.viewDidLoad()

        setupPlayer()
        setupSubcription()
        setupMenuButtonAction()
        setupGestures()
        setupPositionController()

//        #if DEBUG
//        self.player.libraryInstance.debugLogging = true
//        self.player.libraryInstance.debugLoggingLevel = 4
//        #endif

        playbackControlView.isHidden = true
        self.isOpening = true

        let font = UIFont.monospacedDigitSystemFont(ofSize: 30, weight: UIFont.Weight.medium)
        remainingLabel.font = font
        positionLabel.font = font
        scrubbingLabel.font = font
        scrubbingPositionController.player = player

        updateViews(with: player.time)
    }

    private var playerCurrentTime: Double {
        if let value = self.player.time?.value?.doubleValue {
            return value/1000
        }
        return 0
    }
    
    private var playerTotalTime: Double {
        if let value = self.player.totalTime?.value?.doubleValue {
            return value/1000
        }
        return 0.00000001
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.timer?.invalidate()
        self.timer = nil
        self.updateCriteriaToDeatult()
    }

    // MARK: - Setup methods

    private func setupPlayer() {
        self.player.drawable = self.videoView
        self.player.delegate = self
        
        self.timer = ResumableTimer(interval: 0.5, callback: { [weak self] in
            guard let time = self?.player.time.value?.doubleValue else { return }
            self?.playerTimeSubject.send(time)
        })
        self.timer?.start()
    }

    private func setupPlayerMedia(with link: URL) {
        viewModel.link = link
        let media = VLCMedia(url: link)
        media.delegate = self
        player.media = media
    }

    private func setupSubcription() {

        playerStateSubject
            .sink { [weak self] (state) in
                guard let `self` = self else { return }
                Log.write("Player status chagned: \(self.player.state.description.capitalized)")

                self.setupGestures()
                self.setupPositionController()
                self.handlePlaybackControlVisibility()

                switch self.player.state {

                case .stopped:
                    // Menu button tapped
                    if self.viewModel.nextData == nil {
                        self.dismiss()
                    } else {
                        self.isOpening = true
                    }
                    
                case .ended:
                    // The end of the video
                    self.resetNowPlaying()
                    self.viewModel.data.stop(time: self.playerCurrentTime, totalTime: self.playerTotalTime)
                case .error:
                    self.dismiss()
                case .paused:
                    self.updateProgress()
                    self.pauseImageView.isHidden = false
                case .esAdded:
                    break
                case .opening:
                    self.isOpening = true
                case .buffering:
                    self.pauseImageView.isHidden = true
                    guard self.player.isPlaying else { return }
                case .playing: // Did not called
                    self.isOpening = false
                    self.pauseImageView.isHidden = true
                default: break
                }

            }.store(in: &cancelables)

        playerTimeSubject
            .sink { (time) in
                self.timerChanged(time: time)
                self.isBuffering = self.player.state == .buffering
                self.isOpening = self.player.state == .opening
                self.updateNowPlaying()
            }.store(in: &cancelables)


        Publishers.CombineLatest(playerTimeSubject, playerStateSubject)
            .filter { $1 != .error  && self.isHiddenCountDownView()  && self.isTimeToShowNexEpisode }
            .map { _,_ in () }
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                self?.showNextEpisodeView()
            }.store(in: &cancelables)
        
        Publishers.CombineLatest(playerTimeSubject, playerStateSubject)
            .filter { $1 != .error  && self.isHiddenCountDownView() == false  && $1 == .ended }
            .map { _,_ in () }
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                self?.playNext()
            }.store(in: &cancelables)

        errorSubject
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (error) in
                guard let otherError = self?.handleCommonError(error) else { return }
                self?.presentErrorAlert(error: otherError)
            }.store(in: &cancelables)

    }

    private func setupGestures() {
        playPauseGesture.isEnabled = player.state != .opening && player.state != .stopped
        actionGesture.isEnabled = playPauseGesture.isEnabled
    }

    private func setupMenuButtonAction() {
        // NOTE: - VLC Dealloc problem fix
        let menuPressRecognizer = UITapGestureRecognizer()
        menuPressRecognizer.addTarget(self, action: #selector(menuButtonAction(_:)))
        menuPressRecognizer.allowedPressTypes = [NSNumber(value: UIPress.PressType.menu.rawValue)]
        self.view.addGestureRecognizer(menuPressRecognizer)
    }

    private func setupPositionController() {
        guard player.isSeekable && !isOpening && !isPanelDisplayed else {
            positionController = nil
            return
        }

        if player.state == .paused {
            scrubbingPositionController.selectedTime = player.time
            positionController = scrubbingPositionController
        } else {
            positionController = remoteActionPositionController
        }
    }


    // MARK: - Actions
    @IBAction func click(_ sender: LongPressGestureRecogniser) {
        positionController?.click(sender)
    }
    @IBAction func playOrPause(_ sender: Any) {
        positionController?.playOrPause(sender)
    }

    @IBAction func showPanel(_ sender: Any) {
        self.performSegue(withIdentifier: "panel", sender: sender)
        setupPositionController()
        hideControl()
    }

    @objc private func menuButtonAction(_ sender: UITapGestureRecognizer) {
        if player.state == .paused {
            self.player.play()
        } else {
            self.viewModel.nextData = nil
            self.player.stop()
        }
    }

    // MARK: Control
    var playbackControlHideTimer: Timer?
    public func showPlaybackControl() {
        playbackControlHideTimer?.invalidate()
        if player.state != .paused {
            autoHideControl()
        }

        guard self.playbackControlView.isHidden else {
            return
        }
        self.cancelGesture.isEnabled = true

        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.playbackControlView.isHidden = false
        })

    }

    private func autoHideControl() {
        playbackControlHideTimer?.invalidate()
        playbackControlHideTimer = Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { _ in
            self.hideControl()
        }
    }

    private func hideControl() {
        playbackControlHideTimer?.invalidate()
        self.cancelGesture.isEnabled = false

        guard !self.playbackControlView.isHidden else {
            return
        }

        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.playbackControlView.isHidden = true
        })
    }

    @IBAction func cancel(_ sender: Any) {
        player.play()
        hideControl()
    }

    private func timerChanged(time: Double) {
        guard self.player.isPlaying == true else { return }
    
        isOpening = false
        updateViews(with: player.time)
        self.updateCriteria()
        if time > 0 && time < 1000 {
            self.needUpdateCriteria = true
            self.updateVideoLang()
        }

        if let totalTime = self.player.totalTime?.value?.doubleValue {
            let progress = time / totalTime * 100
            if (progress - (self.viewModel.data.progress ?? 0)) > 1 {
                self.updateProgress()
            }
            
           if !self.isHiddenCountDownView() {
                let remain = Float((totalTime - time)/1000)
                if remain > 0, remain < self.viewModel.countDownRepet {
                    let countDown = Float(self.viewModel.countDownRepet - remain)
                    self.updateNextVideo(countDown: countDown/self.viewModel.countDownRepet)
                }
            }
        }
    }
    
    private func updateProgress() {
        self.viewModel.data.update(time: self.playerCurrentTime, totalTime: self.playerTotalTime)
    }
}

//MARK: - Support methods

extension VLCPlayerViewController {

    // MARK: - Play methods

    private func play(stream: VideoStream, isContinue: Bool? = nil) {
        tryFetchSubtitles()
        tryFetchStreamModelAndPlay(of: stream, isContinue: isContinue)
    }

    private func play(link: URL, isContinue: Bool? = nil) {
        self.isOpening = true
        guard let mediaId = viewModel.data.ids?.sccID else { Log.write("[VLCPlayer]: Missing media id!"); return }
        setupPlayerMedia(with: link)

        if let time = viewModel.data.time,
           time != 0,
           let isContinue = isContinue,
           isContinue,
           viewModel.isContrinue
        {
            self.player.time = VLCTime(time*1000)
            self.viewModel.isContrinue = false
        }
        player.play()
        viewModel.appData.scService.setAsPlayed(mediaID: mediaId)
            .assignError(to: errorSubject)
            .sink { (_) in
                Log.write("[VLCPlayer] - Set as played mediaId: \(mediaId)")
            }.store(in: &cancelables)
        
        self.configureNowPlaying()
    }

    private func playNext() {
        guard self.viewModel.nextData != nil else { return }
        self.hideCountDownView(true)
        self.viewModel.updateDataFromNextData()
        
        viewModel.fetchStreams()
            .compactMap {
                $0.isAutoSelectVideo(self.viewModel.continueAudioInLang)
            }.sink(receiveCompletion: { [weak self] completion in
                guard case .failure(let error) = completion else { return }
                self?.presentErrorAlert(error: error)
            }) { [weak self] (videoStrem) in
                self?.play(stream: videoStrem, isContinue: false)
            }.store(in: &cancelables)
    }

    private func createLink(from model: WSStreamModel) -> URL? {
        guard let urlString = model.link else { return nil }
        return URL(string: urlString)
    }


    // MARK: - Show methods

    private func showNextEpisodeView() {
        guard let nextVideo = self.viewModel.nextData, self.isHiddenCountDownView() else { return }
        hideCountDownView(false)
        setNextVideo(title: nextVideo.title)
        setNextVideo(poster: nextVideo.poster)
    }


    private func tryFetchStreamModelAndPlay(of stream: VideoStream, isContinue: Bool? = nil) {
        viewModel
            .getLink(for: stream)
            .compactMap { [weak self] model -> URL? in
                guard let url = self?.createLink(from: model) else { Log.write("[VLCPlayer]: Create URL failed!"); return nil }
                return url
            }
            .sink { [weak self] (completion) in
                guard case .failure(let error) = completion else { return }
                self?.presentErrorAlert(error: error)
            } receiveValue: { [weak self] url in
                Log.write("[VLCPlayer]: Play next succeded.")
                self?.play(link: url, isContinue: isContinue)
            }.store(in: &cancelables)


    }

    private func tryFetchSubtitles() {
        // Try fetch subtitles
        // TODO: Present some better information window with the resutl
        self.viewModel.tryFetchSubtitles()
            .sink(receiveCompletion: { _ in
//                guard case .failure(let error) = completion else { return }
//                self?.presentErrorAlert(error: error) // ak uzivatel nieje prihlaseny tak nepotrebuje vidiet inforemaciu o chybe.
            }) { (subtitles) in
                Log.write("[VLCPlayer]: Fetched subtitles count: \(subtitles.count)")
            }.store(in: &cancelables)
    }



}

// MAKR: - NowUpdate
extension VLCPlayerViewController {
    private func configureNowPlaying() {
        UIApplication.shared.beginReceivingRemoteControlEvents()
        MPRemoteCommandCenter.shared().playCommand.addTarget { [weak self] event in
            self?.player.play()
          return .success
        }
        MPRemoteCommandCenter.shared().pauseCommand.addTarget { [weak self] event in
            self?.player.pause()
          return .success
        }
        
//        MPRemoteCommandCenter.shared().seekForwardCommand.addTarget { [weak self] event in
//            self?.player.jumpForward(Int32(event.timestamp))
//            return .success
//        }
//        MPRemoteCommandCenter.shared().seekBackwardCommand.addTarget { [weak self] event in
//            self?.player.jumpBackward(Int32(event.timestamp))
//            return.success
//        } //changePlaybackPositionCommand
        
        MPRemoteCommandCenter.shared().changePlaybackPositionCommand.addTarget { [weak self] event in
            guard let self = self else { return .commandFailed }
            
            if let event = event as? MPChangePlaybackPositionCommandEvent {
                print("event.timestamp \(event.positionTime)")
                let newTime = VLCTime(event.positionTime*1000)
                self.player.time = newTime
            }
            
            return .success
        }
        
        MPRemoteCommandCenter.shared().enableLanguageOptionCommand.addTarget { event in
            return .success
        }
        
        if self.viewModel.nextData != nil {
            MPRemoteCommandCenter.shared().nextTrackCommand.addTarget { [weak self] _ in
                self?.playNext()
                return.success
            }
        }
        
        self.viewModel.nowPlayingInfo[MPMediaItemPropertyTitle] = self.viewModel.data.title
        
        let url = self.viewModel.data.fanart ?? self.viewModel.data.poster
        if let url = url {
            let resource = ImageResource(downloadURL: url)
            KingfisherManager.shared.retrieveImage(with: resource, options: nil, progressBlock: nil) { result in
                switch result {
                case .success(let value):
                    let art = MPMediaItemArtwork(boundsSize: value.image.size) { size -> UIImage in
                        return value.image
                    }
                    self.viewModel.nowPlayingInfo[MPMediaItemPropertyArtwork] = art
                case .failure(let error):
                    print("Error: \(error)")
                }
            }
        }
        
        DispatchQueue.main.async { [weak self] in
            MPNowPlayingInfoCenter.default().nowPlayingInfo = self?.viewModel.nowPlayingInfo
        }
    }
    
    private func updateNowPlaying() {
        guard self.player.isPlaying else { return }
        self.viewModel.nowPlayingInfo[MPNowPlayingInfoPropertyElapsedPlaybackTime] = self.playerCurrentTime
        self.viewModel.nowPlayingInfo[MPMediaItemPropertyPlaybackDuration] = self.playerTotalTime
        DispatchQueue.main.async { [weak self] in
            MPNowPlayingInfoCenter.default().nowPlayingInfo = self?.viewModel.nowPlayingInfo
        }
    }
    
    private func resetNowPlaying() {
        DispatchQueue.main.async  {
            MPNowPlayingInfoCenter.default().nowPlayingInfo = nil
            UIApplication.shared.endReceivingRemoteControlEvents()
            MPRemoteCommandCenter.shared().playCommand.removeTarget(self)
            MPRemoteCommandCenter.shared().pauseCommand.removeTarget(self)
            MPRemoteCommandCenter.shared().seekForwardCommand.removeTarget(self)
            MPRemoteCommandCenter.shared().seekBackwardCommand.removeTarget(self)
            MPRemoteCommandCenter.shared().nextTrackCommand.removeTarget(self)
        }
    }
}

// MARK: - Update views
extension VLCPlayerViewController {

    fileprivate func updateViews(with time: VLCTime) {
        positionLabel.text = time.stringValue
        updateRemainingLabel(with: time)
        guard let totalTime = player.totalTime,
            let value = time.value?.doubleValue,
            let totalValue = totalTime.value?.doubleValue else {
            remainingLabel.isHidden = true
            positionConstraint.constant = transportBar.bounds.width / 2
            return
        }

        positionConstraint.constant = round(CGFloat(value / totalValue) * transportBar.bounds.width)
        remainingLabel.isHidden = positionConstraint.constant + positionLabel.frame.width > remainingLabel.frame.minX - 60
    }

    fileprivate func updateRemainingLabel(with time: VLCTime) {
        guard let totalTime = player.totalTime, totalTime.value != nil else {
            return
        }
        remainingLabel.text = (totalTime - time).stringValue
    }


    fileprivate func handlePlaybackControlVisibility() {
        if player.state == .paused {
            showPlaybackControl()
        } else {
            autoHideControl()
        }
    }
    
    // MARK: - nexEpisodeView
    public func isHiddenCountDownView() -> Bool {
        return self.continueView.isViewHidden
    }
    
    public func hideCountDownView(_ isHiden: Bool) {
        self.continueView.setHidden(isHiden)
    }

    public func updateNextVideo(countDown value: Float) {
        self.continueView.set(countDown: value)
    }

    public func setNextVideo(title: String?) {
        self.continueView.set(title: title)
    }

    public func setNextVideo(poster url: URL?) {
        if let urlString = url {
            self.continueView.set(poster: urlString)
        }
    }

    private func dismiss(with error: RequestErrors? = nil) {
        self.resetNowPlaying()
        //TODO: Ako osetrit situaciu kedy sa pusta dalsie video
        self.player.stop()
        
        self.viewModel.data.stop(time: self.playerCurrentTime, totalTime: self.playerTotalTime)
        

        if let error = error {
            self.presentErrorAlert(error: error) {
                self.dismiss(animated: true, completion: nil)
            }
            Log.write("VLCController dismiss error: \(error.localizedDescription)")
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
}

// MARK: - VLCMediaPlayerDelegate

extension VLCPlayerViewController: VLCMediaPlayerDelegate {

    public func mediaPlayerStateChanged(_ aNotification: Notification!) {
        playerStateSubject.send(player.state)
    }
    
    public func mediaPlayerTimeChanged(_ aNotification: Notification!) { }
    
    public func mediaPlayerTitleChanged(_ aNotification: Notification!) {}
    public func mediaPlayerChapterChanged(_ aNotification: Notification!) {}
    public func mediaPlayerSnapshot(_ aNotification: Notification!) {}
    public func mediaPlayerStartedRecording(_ player: VLCMediaPlayer!) {}
    public func mediaPlayer(_ player: VLCMediaPlayer!, recordingStoppedAtPath path: String!) {}
}

// MARK: - ScrubbingPositionControllerDelegate

extension VLCPlayerViewController: ScrubbingPositionControllerDelegate {
    func scrubbingPositionController(_ : ScrubbingPositionController, didScrubToTime time: VLCTime) {
        updateRemainingLabel(with: time)
    }

    func scrubbingPositionController(_ : ScrubbingPositionController, didSelectTime time: VLCTime) {
        player.time = time
        updateViews(with: time)
        player.play()
    }
}


// MARK: - Gesture
extension VLCPlayerViewController: UIGestureRecognizerDelegate {
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

// MARK: - PanelViewController
/// PanelViewController - Segue presentation
extension VLCPlayerViewController {

    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? PanelViewController {
            destination.selectedIndex = lastSelectedPanelTabIndex
            destination.player = player
            destination.transitioningDelegate = self
            destination.delegate = self
            displayedPanelViewController = destination
            if let panel = self.displayedPanelViewController as? PanelViewController {
                // tu je potrebne zistovat data zo serialu ktory sa prave prehrava
                panel.mediaInfo = viewModel.infoPanelData
            }
        }
    }

}
/// PanelViewController - Transition animation
extension VLCPlayerViewController: UIViewControllerTransitioningDelegate {

    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return presented is PanelViewController ? SlideDownAnimatedTransitioner() : nil
    }
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return dismissed is PanelViewController ? SlideUpAnimatedTransitioner() : nil
    }
}
/// PanelViewController - Delegate
extension VLCPlayerViewController: PanelViewControllerDelegate {

    func panelViewController(_ panelViewController: PanelViewController, didSelectTabAtIndex index: Int) {
        lastSelectedPanelTabIndex = index
    }

    func panelViewControllerDidDismiss(_ panelViewController: PanelViewController) {
        displayedPanelViewController = nil
        setupPositionController()
        handlePlaybackControlVisibility()
    }
}


/// Subtitles
extension VLCPlayerViewController {

    func addSubtitles() {
        for srt in self.viewModel.osSubtitles {
            if !srt.added {
                guard let url = URL(string: srt.link ?? "") else { return }
                FileDownloader.loadFileAsync(url: url, name: srt.langName) { (path, err) in

                    if err == nil, let path = path {
                        let fileURL = URL(fileURLWithPath: path)
                        self.player.addPlaybackSlave(fileURL, type: .subtitle, enforce: false)
                    }
                }
            }
            if let index = self.viewModel.osSubtitles.firstIndex(where: { $0.link == srt.link}) {
                self.viewModel.osSubtitles[index].added = true
            }
        }
    }

    private func setContinuteLang() {
        let audioIndex = self.player.currentAudioTrackIndex
        if let names = self.player.audioTrackNames,
           let currentName = names[safe: Int(audioIndex)] as? String {
            self.viewModel.continueAudioInLang = AudioLanguage.language(currentName)
        }

        let subtitleIndex = self.player.currentVideoSubTitleIndex
        if subtitleIndex > 0,
           let names = self.player.videoSubTitlesNames,
           let curentSrt = names[Int(subtitleIndex)] as? String {
            self.viewModel.continueTitleInLang = AudioLanguage.language(curentSrt)
        }
    }
    
    func updateVideoLang() {
        if let srtLang = self.viewModel.continueTitleInLang,
           let names = self.player.videoSubTitlesNames as? [String],
           let subtitleIndex = names.firstIndex(where: { AudioLanguage.language($0) == srtLang }),
           self.player.currentVideoSubTitleIndex != subtitleIndex {
            self.player.currentVideoSubTitleIndex = Int32(subtitleIndex)
        }
        if let audioLang = self.viewModel.continueAudioInLang,
           let names = self.player.audioTrackNames as? [String],
           let audioIndex = names.firstIndex(where: { AudioLanguage.language($0) == audioLang }),
           self.player.currentAudioTrackIndex != audioIndex {
               self.player.currentAudioTrackIndex = Int32(audioIndex)
        }
    }
}

/// VLCMediaDelegate
extension VLCPlayerViewController: VLCMediaDelegate {

    public func mediaMetaDataDidChange(_ aMedia: VLCMedia) {
        Log.write("mediaMetaDataDidChange(_ aMedia: VLCMedia)")
        self.addSubtitles()
    }

    public func mediaDidFinishParsing(_ aMedia: VLCMedia) {
        Log.write("mediaDidFinishParsing(_ aMedia: VLCMedia)")
    }
}


// MARK: - The unused staff
extension VLCPlayerViewController {

    public func playerViewLastAudio(_ playerView: VLCPlayerViewController) -> String {
        if let lang = self.viewModel.continueAudioInLang {
            return lang
        }
        if let systemLangiso639 = Locale.current.iso639_2LanguageCode {
            return systemLangiso639
        }
        return "eng"
    }

}


extension VLCMediaPlayerState {

    var description: String {
        switch self {
        case .stopped: return "stopped"
        case .opening: return "opening"
        case .buffering: return "buffering"
        case .ended: return "ended"
        case .error: return "error"
        case .playing: return "playing"
        case .paused: return "paused"
        case .esAdded: return "esAdded"
        default: return "unknown"
        }
    }
}
