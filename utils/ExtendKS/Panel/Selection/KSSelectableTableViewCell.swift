//
//  SelectableTableViewCell.swift
//  TVPlayer
//
//  Created by Jérémy Marchand on 30/12/2018.
//  Copyright © 2018 Jérémy Marchand. All rights reserved.
//

import UIKit

class KSSelectableTableViewCell: UITableViewCell {
    @IBOutlet var label: UILabel!
    @IBOutlet var checkmarkView: UIImageView!
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        super.didUpdateFocus(in: context, with: coordinator)
        if context.nextFocusedView === self {
            coordinator.addCoordinatedAnimations({
                self.label.textColor = .white
            }, completion: nil)
        }
        else {
            coordinator.addCoordinatedAnimations({
                self.label.textColor = .lightGray
            }, completion: nil)
        }
    }
    
    func update(with color: UIColor?) {
        if self.label == nil {
            return
        }
        self.label.textColor = color
        self.checkmarkView.tintColor = color
        if self.checkmarkView.isHidden {
            self.label.font = UIFont.systemFont(ofSize: 31, weight: .medium)
        } else {
            self.label.font = UIFont.systemFont(ofSize: 31, weight: .semibold)
        }
    }
}
