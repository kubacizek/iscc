//
//  SelectorTableViewController.swift
//  TVPlayer
//
//  Created by Jérémy Marchand on 29/12/2018.
//  Copyright © 2018 Jérémy Marchand. All rights reserved.
//

import UIKit
import KSPlayer

class KSSelectorTableViewController: UIViewController {
    var collection: KSSelectableCollection?
    var emptyText: String!

    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var emptyLabel: UILabel!
    @IBOutlet var emptyView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.text = title?.uppercased()
        emptyLabel.text = emptyText
//        tableView.backgroundView = collection.count == 0 ? emptyView : nil
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        scrollToSelectedIndex(animated: false)
    }

    private func scrollToSelectedIndex(animated: Bool = false) {
        if let selectedIndex = collection?.selectedIndex {
            tableView.scrollToRow(at: IndexPath(row: selectedIndex, section: 0), at: .middle, animated: animated)
        }
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        tableView.reloadData()
    }
}

// MARK: Data Source
extension KSSelectorTableViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return nil
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let collection = self.collection {
            return collection.collectionNumberOfRows()
        }
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let collection = self.collection {
            return collection.collection(tableView, cellForRowAt: indexPath)
        }
        fatalError()
    }

    func tableView(_ tableView: UITableView, didUpdateFocusIn context: UITableViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if let indexPath = context.previouslyFocusedIndexPath,
            let cell = tableView.cellForRow(at: indexPath) as? KSSelectableTableViewCell {
            cell.update(with: .lightGray)
        }
        if let nextIndexPath = context.nextFocusedIndexPath,
            let cell = tableView.cellForRow(at: nextIndexPath) as? KSSelectableTableViewCell {
            cell.update(with: .lightGray)
        } else {
            // Reset scroll position when focus leave view
            scrollToSelectedIndex(animated: true)
        }
    }
}

// MARK: Delegate
extension KSSelectorTableViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var indexPathsToRefresh = [indexPath]
        if let previousIndex = self.collection?.selectedIndex {
            indexPathsToRefresh.append(IndexPath(row: previousIndex, section: 0))
        }
        collection?.collection(didSelectRowAt: indexPath)
        tableView.scrollToRow(at: indexPath, at: .middle, animated: true)
        tableView.reloadData()
    }
}
