//
//  PlayerViewController.swift
//  IndexOfTV
//
//  Created by Jérémy Marchand on 24/02/2018.
//  Copyright © 2018 Jérémy Marchand. All rights reserved.
//

import UIKit
import GameController
import KSPlayer
import AVKit

/// `PlayerViewController` is a subclass of `UIViewController` that can be used to display the visual content of an `Player` object and the standard playback controls.
public enum PlayerAction {
    case didEndPlaying
    case prepareNextVersion
}

protocol PlayerViewDelegate: class {
    func playerView(_ playerView: PlayerViewController, didSave info:SccWatched)
    func playerView(_ playerView: PlayerViewController, prepare action:PlayerAction)
    func playerView(_ playerView: PlayerViewController, playerDidTimeChanged: TimeInterval)
    func playerViewWillDisapper(_ playerView: PlayerViewController)
    func playerViewLastAudio(_ playerView: PlayerViewController) -> String
}

public protocol PlayerControllerAudioDelegate {
    func playerControllerChangeAudioRoute()
}

public final class CustomVideoPlayerView: VideoPlayerView {
    public var currentTime: TimeInterval = 0
    public var audioLang: String = "eng"
    public var subtitleLang: String = "none"
    public var audioDelegate: PlayerControllerAudioDelegate?
    
    var externalSubtitles: [OSDownload] = []
    
    public override func customizeUIComponents() {
        super.customizeUIComponents()
        toolBar.isHidden = true
        toolBar.timeSlider.isHidden = true
    }

    override public func player(layer: KSPlayerLayer, state: KSPlayerState) {
        super.player(layer: layer, state: state)
        if state == .readyToPlay, let player = layer.player {
            for track in player.tracks(mediaType: .audio) {
                if track.language == self.audioLang {
                    player.select(track: track)
                }
            }
            if let disableSrt = self.getSubtitles().first {
                self.set(srt: disableSrt)
            }
        }
    }

    public override func onButtonPressed(type: PlayerButtonType, button: UIButton) {
        if type == .landscape {
            // xx
        } else {
            super.onButtonPressed(type: type, button: button)
        }
    }
    
    public func configureSubtitles() {
        let size = SubtitlesSettings.subtitlesSize.rawValue
        self.subtitleLabel.font = UIFont.systemFont(ofSize: CGFloat(size), weight: .medium)
        let color = SubtitlesSettings.subtitlesColor.color
        self.subtitleLabel.textColor = color
    }
    
    public override func seek(time: TimeInterval, completion handler: ((Bool) -> Void)? = nil) {
        super.seek(time: time, completion: handler)
    }
    
    public override func resetPlayer() {
        super.resetPlayer()
    }
    
    deinit {
        self.resetPlayer()
    }
}

//MARK: - Audio
extension CustomVideoPlayerView {
    public func getAudioTracks() -> [MediaPlayerTrack] {
        guard let player = self.playerLayer.player else { return [] }
        return player.tracks(mediaType: .audio)
    }
    
    public func setAudioTrack(_ mediaPlayerTrack: MediaPlayerTrack) {
        guard let player = self.playerLayer.player else { return }
        player.select(track: mediaPlayerTrack)
    }
    
    public func changeAudioRoute() {
        self.audioDelegate?.playerControllerChangeAudioRoute()
    }
}
//MARK: - Subtitles
extension CustomVideoPlayerView {
    public func getSubtitles() -> [KSPlayer.SubtitleInfo] {
        let disableInfo = URLSubtitleInfo(subtitleID: "disabled", name: "disabled".localizable)
        var arraySrt: [KSPlayer.SubtitleInfo] = [disableInfo]
        for srt in self.externalSubtitles {
            if let name = srt.fname, let link = srt.link {
                let srtInfo = URLSubtitleInfo(subtitleID: name, name: name)
                srtInfo.downloadURL = URL(string: link)
                arraySrt.append(srtInfo)
            }
        }
        let srts = srtControl.filterInfos { _ -> Bool in
            return true
        }
        arraySrt.append(contentsOf: srts)
        return arraySrt
    }
    public func getCurrentSubtitles() -> KSPlayer.SubtitleInfo {
        return self.srtControl.view.selectedInfo.wrappedValue
    }
    public func set(srt:KSPlayer.SubtitleInfo) {
        self.srtControl.view.selectedInfo.wrappedValue = srt
    }
}
extension CustomVideoPlayerView {
    public func getSubtitlesTracks() -> [MediaPlayerTrack] {
        guard let player = self.playerLayer.player else { return [] }
        return player.tracks(mediaType: .subtitle)
    }
    
    public func setSubtitleTrack(_ mediaPlayerTrack: MediaPlayerTrack) {
        guard let player = self.playerLayer.player else { return }
        player.select(track: mediaPlayerTrack)
    }
}

class PlayerViewController: UIViewController {
    public static func instantiate() -> PlayerViewController {
        let player = CustomVideoPlayerView()
        return instantiate(player: player)
    }

    public static func instantiate(player: CustomVideoPlayerView) -> PlayerViewController {
        let storyboard = UIStoryboard(name: "TVPlayer", bundle: Bundle(for: PlayerViewController.self))
        guard let controller = storyboard.instantiateInitialViewController() as? PlayerViewController else {
            fatalError()
        }
        controller.player = player
        return controller
    }
    var isTVstram: Bool = false 
    var resource: KSPlayerResource? {
            didSet {
                if let resource = self.resource {
                    self.player?.set(resource: resource)
                    if let lang = self.playerDelegate?.playerViewLastAudio(self) {
                        self.player?.audioLang = lang
                    }
                } else {
                    self.player?.srtControl.searchSubtitle(name: resource?.name ?? "")
                    self.player?.isMaskShow = true
                }
            }
        }
    
    var externalSubtitles: [OSDownload] = [] {
        didSet {
            self.player?.externalSubtitles = self.externalSubtitles
        }
    }
    
    public weak var playerDelegate:PlayerViewDelegate?

    @IBOutlet var videoView: UIView!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var remainingLabel: UILabel!
    @IBOutlet weak var transportBar: KSProgressBar!
    @IBOutlet weak var scrubbingLabel: UILabel!
    @IBOutlet weak var playbackControlView: KSGradientView!
    @IBOutlet weak var rightActionIndicator: UIImageView!

    @IBOutlet weak var positionConstraint: NSLayoutConstraint!
    @IBOutlet weak var bufferingIndicator: UIActivityIndicatorView!

    @IBOutlet var actionGesture: KSLongPressGestureRecogniser!
    @IBOutlet var playPauseGesture: UITapGestureRecognizer!
    @IBOutlet var cancelGesture: UITapGestureRecognizer!

    @IBOutlet var scrubbingPositionController: KSScrubbingPositionController!
    @IBOutlet var remoteActionPositionController: KSRemoteActionPositionController!

    @IBOutlet var continueView: KSContinueView!
    /// Set the player.
    /// The player should be set before the view is loaded.
    public var watched: SccWatched?
    private var mediaInfo: InfoData?
    public var infoPanelData: InfoPanelData?
    private var progress: Float = 0.0

    public weak var player: CustomVideoPlayerView? {
        didSet {
            if isViewLoaded {
                fatalError("The Player player should be set before the view is loaded.")
            }
            isOpening = true
            if self.resource != nil {
                self.play()
            }
            
        }
    }

    private var positionController: KSPositionController? {
        didSet {
            guard positionController !== oldValue else {
                return
            }
            oldValue?.isEnabled = false
            positionController?.isEnabled = true
        }
    }

    private var isOpening: Bool = false {
        didSet {
            guard self.viewIfLoaded != nil else {
                return
            }

            guard isOpening != oldValue else {
                return
            }

            self.setUpPositionController()
        }
    }

    private var isBuffering: Bool = false {
        didSet {
            guard isBuffering != oldValue else {
                return
            }

            if isBuffering {
                bufferingIndicator.startAnimating()
            } else {
                bufferingIndicator.stopAnimating()

            }
            rightActionIndicator.isHidden = isBuffering
        }
    }

    private var lastSelectedPanelTabIndex: Int = 0
    private var displayedPanelViewController: UIViewController?
    private var isPanelDisplayed: Bool {
        return displayedPanelViewController != nil
    }
    private(set) var state:KSPlayerState?

    public override func viewDidLoad() {
        super.viewDidLoad()
        self.setupPlayer()
        self.player?.delegate = self

        playbackControlView.isHidden = true

        let font = UIFont.monospacedDigitSystemFont(ofSize: 30, weight: UIFont.Weight.medium)
        remainingLabel.font = font
        positionLabel.font = font
        scrubbingLabel.font = font

        scrubbingPositionController.player = player
        self.scrubbingPositionController.addPanGesture()

        setUpGestures()
        setUpPositionController()
        animateIndicatorsIfNecessary()
    }
    
    private func setupPlayer() {
        guard let player = self.player else { return }
        self.view.addSubview(player)
        self.view.sendSubviewToBack(player)
        player.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            player.topAnchor.constraint(equalTo: view.topAnchor),
            player.leftAnchor.constraint(equalTo: view.leftAnchor),
            player.rightAnchor.constraint(equalTo: view.rightAnchor),
            player.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
        self.view.layoutIfNeeded()
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.playerDelegate?.playerViewWillDisapper(self)
    }

    deinit {
        self.updateWatched()
        self.watched?.stop(progress: self.progress)
    }

    public override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: IB Actions
    @IBAction func click(_ sender: KSLongPressGestureRecogniser) {
        positionController?.click(sender)
    }
    @IBAction func playOrPause(_ sender: Any) {
        positionController?.playOrPause(sender)
    }

    @IBAction func showPanel(_ sender: Any) {
        self.performSegue(withIdentifier: "panel", sender: sender)
        setUpPositionController()
        hideControl()
    }

    // MARK: - Control
    var playbackControlHideTimer: Timer?
    public func showPlaybackControl() {
        playbackControlHideTimer?.invalidate()
        if self.state != .paused {
            autoHideControl()
        }

        guard self.playbackControlView.isHidden else {
            return
        }
        self.cancelGesture.isEnabled = true

        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.playbackControlView.isHidden = false
        })

    }

    private func autoHideControl() {
        playbackControlHideTimer?.invalidate()
        playbackControlHideTimer = Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { _ in
            self.hideControl()
        }
    }

    private func hideControl() {
        playbackControlHideTimer?.invalidate()
        self.cancelGesture.isEnabled = false

        guard !self.playbackControlView.isHidden else {
            return
        }

        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.playbackControlView.isHidden = true
        })
    }

    @IBAction func cancel(_ sender: Any) {
        self.play()
        hideControl()
    }
}

//MARK: - KSPlayer Delegate
extension PlayerViewController: PlayerControllerDelegate {
    public func playerController(state: KSPlayerState) {
        self.state = state
        setUpGestures()
        setUpPositionController()
        animateIndicatorsIfNecessary()
        handlePlaybackControlVisibility()
        
        if state == .playedToTheEnd || state == .error {
            self.updateWatched()
            self.playerDelegate?.playerView(self, prepare: .didEndPlaying)
        }
    }
    
    public func playerController(currentTime: TimeInterval, totalTime: TimeInterval) {
        self.player?.currentTime = currentTime
        self.isOpening = false
        self.isBuffering = false

        self.playerDelegate?.playerView(self, playerDidTimeChanged: currentTime)
        let total = Int(totalTime)
        if currentTime > 0,
           (Int(currentTime) + Int(totalTime * 0.1)) > total {
            self.updateWatched()
        }
        
        guard currentTime != 0, totalTime != 0 else { return } //ochrana koli live streamu
        self.watched?.time = currentTime
        self.updateViews(with: currentTime)
        let currentProgress = Int(currentTime / totalTime * 100)
        if Int(self.progress) + 1 <= currentProgress {
            self.progress = Float(currentProgress)
            self.watched?.update(progress: Float(currentProgress))
        }
    }
    
    public func playerController(finish error: Error?) {
        self.playerDelegate?.playerView(self, prepare: .didEndPlaying)
    }
    
    public func playerController(maskShow: Bool) {
        
    }
    
    public func playerController(action: PlayerButtonType) {
        
    }
    
    public func playerController(bufferedCount: Int, consumeTime: TimeInterval) {
        
    }
}

extension PlayerViewController: PlayerControllerAudioDelegate {
    public func playerControllerChangeAudioRoute() {
        let routePickerButton = AVRoutePickerView(frame: CGRect(origin: .zero, size: CGSize(width: 200, height: 200)))
        if let routePickerButton = routePickerButton.subviews.first(where: { $0 is UIButton }) as? UIButton {
            routePickerButton.sendActions(for: .primaryActionTriggered)
        }
    }
}

//MARK: - iSCC Methods
extension PlayerViewController {
    public func play(watched info: SccWatched, mediaInfo: InfoData, Media: TimeInterval, isContinue: Bool) {
        self.watched = info
        self.mediaInfo = mediaInfo
        self.play()
        if isContinue {
            self.jupTo(time: info.time)
        }
    }
    
    private func jupTo(time: TimeInterval) {
        if time != 0 {
            self.player?.seek(time: time, completion: { [weak self] _ in
                guard let self = self else { return }
                self.play()
            })
        }
    }
    
    public func hideNextVideoView(_ isHiden: Bool) {
        self.continueView.isHidden = isEditing
    }
    
    public func updateNextVideo(countDown value: Int) {
        self.continueView.set(countDown: "\(value)")
    }
    
    public func setNextVideo(title: String?) {
        self.continueView.set(title: title)
    }
    
    public func setNextVideo(poster url: String?) {
        if let urlString = url {
            self.continueView.set(poster: urlString)
        }
    }
}

//MARK: - support methods

extension PlayerViewController {
    public func play(watched info: SccWatched, mediaInfo: InfoData?, isContinue: Bool) {
        self.mediaInfo = mediaInfo
        self.watched = info
        if isContinue {
            player?.seek(time: info.time, completion: { [weak self] _ in
                guard let self = self else { return }
                self.play()
            })
        } else {
            self.play()
        }
    }

    private func updateWatched() {
        if var wathced = self.watched {
            guard let player = self.player else { return }
            
            let total = Int(player.totalTime)
            if player.currentTime > 0,
               (Int(player.currentTime) + Int(player.totalTime * 0.1)) > total {
                wathced.isSeen = true
                wathced.time = 0
            } else if let time = self.player?.currentTime {
                wathced.isSeen = false
                wathced.time = time
            }
            self.playerDelegate?.playerView(self, didSave: wathced)
        }
    }
    
    public func hideCountDownView(_ isHiden: Bool) {
        self.continueView.isHidden = isHiden
    }

    public func setCountDownImage(poster url: String?) {
        if let urlString = url {
            self.continueView.set(poster: urlString)
        }
    }
    
    private func play() {
        if self.resource != nil {
            self.player?.configureSubtitles()
            self.player?.play()
            self.continueView?.isHidden = true
        }
    }
}

// MARK: - Update views
extension PlayerViewController {
    fileprivate func updateViews(with time: TimeInterval) {
        
        guard let player = self.player else { return }
        positionLabel.text = time.toString(for: .minOrHour)
        updateRemainingLabel(with: time)
        positionConstraint.constant = round(CGFloat(time / player.totalTime) * transportBar.bounds.width)
        remainingLabel.isHidden = positionConstraint.constant + positionLabel.frame.width > remainingLabel.frame.minX - 60
    }

    fileprivate func updateRemainingLabel(with time: TimeInterval) {
        guard let player = self.player else { return }
        remainingLabel.text = (player.totalTime - time).toString(for: .minOrHour)
    }

    fileprivate func setUpPositionController() {
        guard !isOpening && !isPanelDisplayed else {
            positionController = nil
            return
        }

        if self.state == .paused {
            guard let player = self.player else { return }
            scrubbingPositionController.selectedTime = player.currentTime
            positionController = scrubbingPositionController
        } else {
            positionController = remoteActionPositionController
        }
    }

    fileprivate func animateIndicatorsIfNecessary() {
        guard let state = self.state else { return }
        if state == .buffering && state.isPlaying {
            isBuffering = true
        }
    }

    fileprivate func setUpGestures() {
        guard let state = self.state else { return }
        if self.isTVstram {
            playPauseGesture.isEnabled = false
            actionGesture.isEnabled = false
        } else {
            playPauseGesture.isEnabled = !self.isOpening && state != .playedToTheEnd
            actionGesture.isEnabled = playPauseGesture.isEnabled
        }
    }

    fileprivate func handlePlaybackControlVisibility() {
        if self.state == .paused {
            showPlaybackControl()
        } else {
            autoHideControl()
        }
    }
}

// MARK: - Scrubbling Delegate
extension PlayerViewController: KSScrubbingPositionControllerDelegate {
    func scrubbingPositionController(_ : KSScrubbingPositionController, didScrubToTime time: TimeInterval) {
        updateRemainingLabel(with: time)
    }

    func scrubbingPositionController(_ : KSScrubbingPositionController, didSelectTime time: TimeInterval) {
        guard let player = self.player else { return }
        if player.currentTime != time {
            player.seek(time: time) { [weak self] _ in
                guard let self = self else { return }
                self.play()
            }
        } else {
            self.play()
        }
        updateViews(with: time)
    }
}

// MARK: - Remote Action Delegate
extension PlayerViewController: KSRemoteActionPositionControllerDelegate {
    func remoteActionPositionControllerDidDetectTouch(_ : KSRemoteActionPositionController) {
        showPlaybackControl()
    }
    func remoteActionPositionController(_ : KSRemoteActionPositionController, didSelectAction action: KSRemoteActionPositionController.Action) {
        showPlaybackControl()
        guard let player = self.player else { return }
        if action == .pause {
            player.pause()
        } else if action == .reset {
            player.resetPlayer()
        }
    }
}

// MARK: - Gesture
extension PlayerViewController: UIGestureRecognizerDelegate {
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

// MARK: - Panel
extension PlayerViewController {
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? KSPanelViewController {
            destination.selectedIndex = lastSelectedPanelTabIndex
            destination.player = player
            destination.transitioningDelegate = self
            destination.delegate = self
            displayedPanelViewController = destination
            if let panel = self.displayedPanelViewController as? KSPanelViewController,
                let panelData = self.infoPanelData {
                panel.mediaInfo = panelData
            }
        }
    }
}

extension PlayerViewController: UIViewControllerTransitioningDelegate {
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return presented is KSPanelViewController ? KSSlideDownAnimatedTransitioner() : nil
    }
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return dismissed is KSPanelViewController ? SlideUpAnimatedTransitioner() : nil
    }
}

extension PlayerViewController: KSPanelViewControllerDelegate {
    func panelViewController(_ panelViewController: KSPanelViewController, didSelectTabAtIndex index: Int) {
        lastSelectedPanelTabIndex = index
    }

    func panelViewControllerDidDismiss(_ panelViewController: KSPanelViewController) {
        displayedPanelViewController = nil
        setUpPositionController()
        handlePlaybackControlVisibility()
    }
}
