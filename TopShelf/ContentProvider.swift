//
//  ContentProvider.swift
//  TopShelf
//
//  Created by SCC on 26/05/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import TVServices
import UIKit
import Combine

final class ContentProvider: TVTopShelfContentProvider {

    private let scService: TopShelfSC2Service = TopShelfSC2Service()
    var movieItems:[TVTopShelfSectionedItem]?
    var tvShowItems:[TVTopShelfSectionedItem]?

    var completition:((TVTopShelfContent?) -> Void)?
    
    private var cancelables: Set<AnyCancellable> = Set()
    private let errorSubject: PassthroughSubject<Error, Never> = PassthroughSubject()

    override func loadTopShelfContent(completionHandler: @escaping (TVTopShelfContent?) -> Void) {
        // Fetch content and call completionHandler
        self.completition = completionHandler
        self.getData()
    }

    func getData() {

        self.scService.getTopShelfData(type: .movie)
            .assignError(to: errorSubject)
            .sink { [weak self] data in
                let filtredData = data.data.filter({ $0.poster != nil })
                self?.movieItems = filtredData.map({ $0.makeSectionItem(.movie) })
                self?.completitionData()
            }.store(in: &cancelables)
        
        self.scService.getTopShelfData(type: .tvshow)
            .assignError(to: errorSubject)
            .sink { [weak self] data in
                let filtredData = data.data.filter({ $0.poster != nil })
                self?.tvShowItems = filtredData.map({ $0.makeSectionItem(.movie) })
                self?.completitionData()
            }.store(in: &cancelables)
    }

    private func completitionData() {
        guard let movieItems = self.movieItems,
            let tvShowItems = self.tvShowItems else { return }

        var content:TVTopShelfSectionedContent? = nil
        var array:[TVTopShelfItemCollection<TVTopShelfSectionedItem>] = []
        if movieItems.count > 0 {
            let itemMovieCollection = TVTopShelfItemCollection(items: movieItems)
            itemMovieCollection.title = String(localized: .movies)
            array.append(itemMovieCollection)
        }

        if tvShowItems.count > 0 {
            let itemTvShowCollection = TVTopShelfItemCollection(items: tvShowItems)
            itemTvShowCollection.title = String(localized: .tvShows)
            array.append(itemTvShowCollection)
        }

        if array.count > 0 {
            content = TVTopShelfSectionedContent(sections: array )
        }

        if let completionHandler = self.completition {
            completionHandler(content)
        }
    }
}

extension SCCMovie {
    func makeSectionItem(_ type: FilterType) -> TVTopShelfSectionedItem {
        guard let identifier = self.ids?.sccID else { fatalError() }
        let item = TVTopShelfSectionedItem(identifier: identifier)
        item.title = self.title

        if let imagePath = self.poster {
            item.setImageURL(imagePath, for: .screenScale2x)
        }

        var services:String = "?services=none"

        if let servicesQuery = self.servicesAsQuery {
            services = servicesQuery
        }

        item.playAction = URL(string: "scc://scc/openIn\(services)&type=\(type.rawValue)&movieID=\(identifier)&startPlaying=true").map { TVTopShelfAction(url: $0) }
        item.displayAction = URL(string: "scc://scc/openIn\(services)&type=\(type.rawValue)&movieID=\(identifier)&startPlaying=false").map { TVTopShelfAction(url: $0) }

        return item
    }
    
    var servicesAsQuery:String? {
        get {
            var services:[String:String] = [:]
            if let csfd = self.ids?.csfd {
                services["csfd"] = "\(csfd)"
            }
            if let imdb =  self.ids?.imdb {
                services["imdb"] = imdb
            }
            if let trakt =  self.ids?.trakt {
                services["trakt"] = "\(trakt)"
            }
            if let tvdb =  self.ids?.tvdb {
                services["tvdb"] = "\(tvdb)"
            }
            if let tmdb =  self.ids?.tmdb {
                services["tmdb"] = "\(tmdb)"
            }
            if let fanart = fanart {
                services["fanart"] = fanart.absoluteString
            }
            return self.queryString(params: services)
        }
    }
    
    func queryString(params: [String: String]) -> String? {
        var components = URLComponents()
        components.queryItems = params.map { element in URLQueryItem(name: element.key, value: element.value) }

        return components.url?.absoluteString
    }
}
