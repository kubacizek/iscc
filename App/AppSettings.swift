//
//  File.swift
//  StreamCinema.atv
//
//  Created by SCC on 31/01/2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit

final public class CurrentAppSettings {
    private static let PassCodeKey = "AppPassCode"
    
    private static var appData:AppData? {
        get {
            return (UIApplication.shared.delegate as? AppDelegate)?.appData
        }
    }

    static var passCode: String? {
        get {
            return UserDefaults.standard.string(forKey: CurrentAppSettings.PassCodeKey)
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: CurrentAppSettings.PassCodeKey)
            if newValue == nil {
                self.appData?.isDisableAdult = false
            } else {
                self.appData?.isDisableAdult = true
            }
            UserDefaults.standard.synchronize()
        }
    }
    
    static var isPassCodeEnabled: Bool {
        get {
            if CurrentAppSettings.passCode != nil {
                return true
            }
            return false
        }
    }
}
