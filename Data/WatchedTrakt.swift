//
//  WatchedTrakt.swift
//  StreamCinema.atv
//
//  Created by SCC on 17/01/2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit
import CoreData

protocol WatchedObject {
    static func getMovie(_ movieID:String?, entity: WatchedEntity) -> SCCMovie?
    static func getEpisode(_ episodeID:String?, entity: WatchedEntity) -> SCCMovie?
    static func getShow(_ showID:String?, entity: WatchedEntity) -> SCCMovie?
    static func getAllEpisodes(for seasonID:String?, entity: WatchedEntity) -> [SCCMovie]
}

/// entity for trakt: "Trakt"
/// entity for scc: "Scc"
public enum WatchedEntity: String {
    case trakt = "Trakt"
    case scc = "Scc"
    case sccWatchList = "SccWatchList"
}

final class WatchedData: WatchedObject {
    private static func getContext() -> NSManagedObjectContext? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return nil }
        return appDelegate.persistentContainer.viewContext
    }
    
    private static func getContainer() -> NSPersistentStoreCoordinator? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return nil }
        return appDelegate.persistentContainer.persistentStoreCoordinator
    }
    
    static func getMovie(_ movieID:String?, entity: WatchedEntity) -> SCCMovie? {
        guard let movieID = movieID else { return nil }
        if let managedContext = self.getContext() {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: entity.rawValue)
            switch entity {
            case .trakt:
                request.predicate = NSPredicate(format: "tvshowTrakt = %@ AND type = %@", movieID, SCCType.movie.rawValue)
            case .scc,
                 .sccWatchList:
                request.predicate = NSPredicate(format: "tvshowScc = %@ AND type = %@", movieID, SCCType.movie.rawValue)
            }

            request.returnsObjectsAsFaults = false
            do {
                let result = try managedContext.fetch(request)
                if let result = result.first as? NSManagedObject {
                    var movie = SCCMovie(with: result)
                    if entity == .trakt {
                        movie.progress = 0
                        movie.time = 0
                    }
                    return movie
                }
            } catch {
                Log.write(error.localizedDescription)
            }
        }
        return nil
    }
    
    static func getShow(_ showID: String?, entity: WatchedEntity) -> SCCMovie? {
        guard let showID = showID else { return nil }
        if let managedContext = self.getContext() {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: entity.rawValue)
            switch entity {
            case .trakt:
                request.predicate = NSPredicate(format: "tvshowTrakt = %@ AND type = %@", showID, SCCType.tvshow.rawValue)
            case .scc,
                 .sccWatchList:
                request.predicate = NSPredicate(format: "tvshowScc = %@ AND type = %@", showID, SCCType.tvshow.rawValue)
            }
            request.returnsObjectsAsFaults = false
            do {
                let result = try managedContext.fetch(request)
                if let result = result.first as? NSManagedObject {
                    return SCCMovie(with: result)
                }
            } catch {
                Log.write(error.localizedDescription)
            }
        }
        return nil
    }
    
    static func lastEpisode(showID:String?, entity: WatchedEntity) -> SCCMovie? {
        guard let showID = showID,
              let managedContext = self.getContext()
        else { return nil }
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entity.rawValue)
        switch entity {
        case .trakt:
            request.predicate = NSPredicate(format: "tvshowTrakt = %@", showID)
        case .scc,
             .sccWatchList:
            request.predicate = NSPredicate(format: "tvshowScc = %@ AND traktDate != nil", showID)
        }
        request.returnsObjectsAsFaults = false
        request.fetchLimit = 1
        let sort = NSSortDescriptor(key: "traktDate", ascending: true)
        request.sortDescriptors = [sort]
        do {
            let results = try managedContext.fetch(request)
            if let result = results.first as? NSManagedObject {
                return SCCMovie(with: result)
            }
        } catch {
            Log.write(error.localizedDescription)
        }
        return nil
    }
    
    static func getSeason(_ seasonID:String?, entity: WatchedEntity) -> SCCMovie? {
        guard let seasonID = seasonID else { return nil }
        if let managedContext = self.getContext() {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: entity.rawValue)
            switch entity {
            case .trakt:
                request.predicate = NSPredicate(format: "seasonTrakt = %@", seasonID)
            case .scc,
                 .sccWatchList:
                request.predicate = NSPredicate(format: "seasonScc = %@", seasonID)
            }
            request.returnsObjectsAsFaults = false
            do {
                let result = try managedContext.fetch(request)
                if let result = result.first as? NSManagedObject {
                    var movie = SCCMovie(with: result)
                    if entity == .trakt {
                        movie.progress = 0
                        movie.time = 0
                    }
                    return movie
                }
            } catch {
                Log.write(error.localizedDescription)
            }
        }
        return nil
    }
    
    static func getEpisode(_ episodeID:String?, entity: WatchedEntity) -> SCCMovie? {
        guard let episodeID = episodeID else { return nil }
        if let managedContext = self.getContext() {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: entity.rawValue)
            switch entity {
            case .trakt:
                request.predicate = NSPredicate(format: "episodeTrakt = %@", episodeID)
            case .scc,
                 .sccWatchList:
                request.predicate = NSPredicate(format: "episodeScc = %@", episodeID)
            }
            request.returnsObjectsAsFaults = false
            do {
                let result = try managedContext.fetch(request)
                if let result = result.first as? NSManagedObject {
                    var movie = SCCMovie(with: result)
                    if entity == .trakt {
                        movie.progress = 0
                        movie.time = 0
                    }
                    return movie
                }
            } catch {
                Log.write(error.localizedDescription)
            }
        }
        return nil
    }
  

    static func getAllEpisodes(for seasonID:String?, entity: WatchedEntity) -> [SCCMovie] {
        guard let seasonID = seasonID else { return [] }
        if let managedContext = self.getContext() {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: entity.rawValue)
            switch entity {
            case .trakt:
                request.predicate = NSPredicate(format: "tvshowTrakt = %@ AND type = %@", seasonID, SCCType.tvshow.rawValue)
            case .scc,
                 .sccWatchList:
                request.predicate = NSPredicate(format: "tvshowScc = %@ AND type = %@", seasonID, SCCType.tvshow.rawValue)
            }
            
            request.returnsObjectsAsFaults = false
            do {
                let result = try managedContext.fetch(request)
                var episodes:[SCCMovie] = []
                if let result = result as? [NSManagedObject] {
                    for item in result {
                        let traktEpisode = SCCMovie(with: item)
                        episodes.append(traktEpisode)
                    }
                    return episodes
                }
            } catch {
                Log.write(error.localizedDescription)
            }
        }
        return []
    }
    
    static func existWatchedEpisode(for showID:String?, entity: WatchedEntity) -> Bool {
        guard let showID = showID else { return false }
        if let managedContext = self.getContext() {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: entity.rawValue)
            switch entity {
            case .trakt:
                request.predicate = NSPredicate(format: "tvshowTrakt = %@ AND type = %@", showID, SCCType.episode.rawValue)
            case .scc,
                 .sccWatchList:
                request.predicate = NSPredicate(format: "tvshowScc = %@ AND type = %@", showID, SCCType.episode.rawValue)
            }
            request.fetchLimit = 1
            request.returnsObjectsAsFaults = false
            do {
                let result = try managedContext.fetch(request)
                if let resultObject = result.first as? NSManagedObject {
                    let _ = SCCMovie(with: resultObject)
                    return true
                }
            } catch {
                Log.write(error.localizedDescription)
            }
        }
        return false
    }
    
    public static func deleteAllData(for entity:WatchedEntity = .scc) {
        if let managedContext = self.getContext(),
            let persistentContainer = self.getContainer() {
            
            let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entity.rawValue)
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)

            do {
                try persistentContainer.execute(deleteRequest, with: managedContext)
            } catch let error as NSError {
                Log.write(String(format:"Detele all data in \(entity) error :", error.localizedDescription))
            }
        }
    }
    
    private static func fetchAll(_ type:SCCType, entity: WatchedEntity) -> [SCCMovie] {
        if let managedContext = self.getContext() {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: entity.rawValue)
            request.predicate = NSPredicate(format: "type = %@", type.rawValue)
            request.returnsObjectsAsFaults = false
            let sort = NSSortDescriptor(key: "traktDate", ascending: true)
            request.sortDescriptors = [sort]
            do {
                let result = try managedContext.fetch(request)
                var episodes:[SCCMovie] = []
                if let result = result as? [NSManagedObject] {
                    for item in result {
                        let traktEpisode = SCCMovie(with: item)
                        episodes.append(traktEpisode)
                    }
                    return episodes
                }
            } catch {
                Log.write(error.localizedDescription)
            }
        }
        return []
    }
}

//MARK: - History
extension WatchedData {
    static func getAllHistoryData(for type:FilterType, entity: WatchedEntity) -> [SCCMovie] {
        if type == .movie {
            return self.getAllMovies(entity: entity)
        } else if type == .tvshow, entity == .sccWatchList {
            return self.getAllShows(entity: entity, type: .tvshow)
        } else if type == .tvshow {
            return self.getAllShows(entity: entity)
        } else {
            return []
        }
    }
    
    static func getAllMovies(entity: WatchedEntity)  -> [SCCMovie] {
        let movies =  WatchedData.fetchAll(.movie, entity: entity)
        return self.sortAndFilterDuplicates(movies)
    }
    
    static func getAllShows(entity: WatchedEntity, type:SCCType = .episode) -> [SCCMovie] {
        let shows:[SCCMovie]
        if entity == .sccWatchList && type == .episode {
            shows = WatchedData.fetchAll(.tvshow, entity: entity)
        } else {
            shows = WatchedData.fetchAll(type, entity: entity)
        }
        
        return self.sortAndFilterDuplicates(shows)
    }
    
    private static func sortAndFilterDuplicates(_ data:[SCCMovie]) -> [SCCMovie] {
        let sorted = data.sorted { (leftW, rightW) -> Bool in
            guard let leftLast = leftW.date, let rightLast = rightW.date else { return true }
            return  leftLast > rightLast
        }
        let filtredResult = sorted.filterDuplicates { (lhs, rhs) -> Bool in
            if let lhsRootID = lhs.rootIDs,
               let rhsRootID = rhs.rootIDs,
               lhsRootID == rhsRootID {
                return true
            }
            return false
        }
        return filtredResult
    }
}
